# ForumProject
Forum project for web dev server side module.

To use the forum, a user must register and log in.
Users can create threads and posts, edit or delete their own thread/post and vote on other user's threads & posts.
A user can change their profile picture, change password or delete their account.

Admin can perform all the same actions as user, withe the addition of creating/editing/deleting categories and being able to
edit and delete any user's thread or post.


## How to run it
* clone the repository
* run *composer install*
* create a database
* in the new database, paste in the contents of db/forum.sql and run the command
* change the values in app/db_config.php to match your configuration
* make the public folder the server document root
* may need to change folder owner (for me it was *sudo chown -R www-data:www-data Forum*)
* reload server (for me *sudo service apache2 reload*)
* go to localhost in your browser and everything should be working
