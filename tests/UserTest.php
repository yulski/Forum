<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 19/04/16
 * Time: 15:07
 */

namespace ForumTest;

use Forum\Table\User;

class UserTest extends \PHPUnit_Framework_TestCase
{

    public function testCanCreateNewObject()
    {
        // Arrange
        $user = new User();

        // Act

        // Assert
        $this->assertNotNull($user);
    }

    public function testGetSetIdReturnsId()
    {
        // Arrange
        $user = new User();
        $user->setId(8);
        $expectedResult = 8;

        // Act
        $id = $user->getId();

        // Assert
        $this->assertEquals($expectedResult, $id);
    }

    public function testGetSetUsernameReturnsUsername()
    {
        // Arrange
        $user = new User();
        $user->setUsername('username');
        $expectedResult = 'username';

        // Act
        $username = $user->getUsername();

        // Assert
        $this->assertEquals($expectedResult, $username);
    }

    public function testGetSetEmailReturnsEmail()
    {
        // Arrange
        $user = new User();
        $user->setEmail('user@gmail.com');
        $expectedResult = 'user@gmail.com';

        // Act
        $email = $user->getEmail();

        // Assert
        $this->assertEquals($expectedResult, $email);
    }

    public function testGetSetPasswordReturnsPassword()
    {
        // Arrange
        $user = new User();
        $user->setPassword('password');
        $expectedResult = 'password';

        // Act
        $password = $user->getPassword();

        // Assert
        $this->assertEquals($expectedResult, $password);
    }

    public function testGetSetDateJoinedReturnsDateJoined()
    {
        // Arrange
        $user = new User();
        $date = date('Y-m-d');
        $user->setDateJoined($date);
        $expectedResult = $date;

        // Act
        $dateJoined = $user->getDateJoined();

        // Assert
        $this->assertEquals($expectedResult, $dateJoined);
    }

    public function testGetSetAdministratorReturnsAdministrator()
    {
        // Arrange
        $user = new User();
        $user->setAdministrator(0);
        $expectedResult = 0;

        // Act
        $administrator = $user->getAdministrator();

        // Assert
        $this->assertEquals($expectedResult, $administrator);
    }

    public function testGetSetReputationReturnsReputation()
    {
        // Arrange
        $user = new User();
        $user->setReputation(10);
        $expectedResult = 10;

        // Act
        $reputation = $user->getReputation();

        // Assert
        $this->assertEquals($expectedResult, $reputation);
    }

    public function testGetSetPictureReturnsPicture()
    {
        // Arrange
        $user = new User();
        $user->setPicture('picture.jpg');
        $expectedResult = 'picture.jpg';

        // Act
        $picture = $user->getPicture();

        // Assert
        $this->assertEquals($expectedResult, $picture);
    }

    public function testAddReputationIncreasesReputation()
    {
        // Arrange
        $user = new User();
        $user->setReputation(5);
        $user->addReputation();
        $expectedResult = 6;

        //Act
        $reputation = $user->getReputation();

        // Assert
        $this->assertEquals($expectedResult, $reputation);
    }

    public function testLoseReputationDecresesReputation()
    {
        // Arrange
        $user = new User();
        $user->setReputation(4);
        $user->loseReputation();
        $expectedResult = 3;

        // Act
        $reputation = $user->getReputation();

        // Assert
        $this->assertEquals($expectedResult, $reputation);
    }

    /**
     * @dataProvider usernameDataProvider
     */
    public function testValidateUsername($username, $expectedMessage, $expectException)
    {
        $exceptionThrown = false;
        $message = '';

        try {
            User::validateUsername($username);
        } catch (\Exception $e) {
            $exceptionThrown = true;
            $message = $e->getMessage();
        } finally {
            if ($expectException) {
                $this->assertEquals($expectedMessage, $message);
            } else {
                $this->assertFalse($exceptionThrown);
            }
        }
    }

    /**
     * @dataProvider emailDataProvider
     */
    public function testValidateEmailAddress($emailAddress, $expectedMessage, $expectException)
    {
        $exceptionThrown = false;
        $message = '';

        try {
            User::validateEmail($emailAddress);
        } catch (\Exception $e) {
            $exceptionThrown = true;
            $message = $e->getMessage();
        } finally {
            if ($expectException) {
                $this->assertEquals($expectedMessage, $message);
            } else {
                $this->assertFalse($exceptionThrown);
            }
        }
    }

    /**
     * @dataProvider passwordDataProvider
     */
    public function testValidatePassword($password, $expectedMessage, $expectException)
    {
        $exceptionThrown = false;
        $message = '';

        try {
            User::validatePassword($password);
        } catch (\Exception $e) {
            $exceptionThrown = true;
            $message = $e->getMessage();
        } finally {
            if ($expectException) {
                $this->assertEquals($expectedMessage, $message);
            } else {
                $this->assertFalse($exceptionThrown);
            }
        }
    }

    public function emailDataProvider()
    {
        return array(
            array('', 'Email is empty', true),
            array('a@a.a', 'Email is too short', true),
            array('1234', 'Email is not in valid email format.', true),
            array('valid123@yahoo.com', '', false)
        );
    }

    public function usernameDataProvider()
    {
        return array(
            array('', 'Username is empty', true),
            array('a', 'Username is too short', true),
            array('test username test username test username', 'Username is too long', true),
            array('valid_username', '', false)
        );
    }

    public function passwordDataProvider()
    {
        return array(
            array('', 'Password is empty', true),
            array('a', 'Password is too short', true),
            array('password_password_password_password_password_password_password_password_password_password_password_password_password_password_password_password_password_password_password_password_password_password_password_password_password_password_password_password_password_password', 'Password is too long', true),
            array('validpassword', '', false)
        );
    }
}
