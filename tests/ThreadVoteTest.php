<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 19/04/16
 * Time: 15:02
 */

namespace ForumTest;

use Forum\Table\ThreadVote;

class ThreadVoteTest extends \PHPUnit_Framework_TestCase
{
    public function testCanCreateObject()
    {
        // Arrange
        $threadVote = new ThreadVote();

        // Act

        // Assert
        $this->assertNotNull($threadVote);
    }

    public function testGetSetIdReturnsId()
    {
        // Arrange
        $threadVote = new ThreadVote();
        $threadVote->setId(2);
        $expectedResult = 2;

        // Act
        $id = $threadVote->getId();

        // Assert
        $this->assertEquals($expectedResult, $id);
    }

    public function testGetSetUserReturnsUser()
    {
        // Arrange
        $threadVote = new ThreadVote();
        $threadVote->setUser(2);
        $expectedResult = 2;

        // Act
        $user = $threadVote->getUser();

        // Assert
        $this->assertEquals($expectedResult, $user);
    }

    public function testGetSetThreadReturnsThread()
    {
        // Arrange
        $threadVote = new ThreadVote();
        $threadVote->setThread(3);
        $expectedResult = 3;

        // Act
        $thread = $threadVote->getThread();

        // Assert
        $this->assertEquals($expectedResult, $thread);
    }

    public function testGetSetVoteReturnVote()
    {
        // Arrange
        $threadVote = new ThreadVote();
        $threadVote->setVote(0);
        $expectedResult = 0;

        // Act
        $vote = $threadVote->getVote();

        // Assert
        $this->assertEquals($expectedResult, $vote);
    }
}
