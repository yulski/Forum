<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 19/04/16
 * Time: 14:55
 */

namespace ForumTest;

use Forum\Table\PostVote;

class PostVoteTest extends \PHPUnit_Framework_TestCase
{

    public function testCanCreateObject()
    {
        // Arrange
        $postVote = new PostVote();

        // Act

        // Assert
        $this->assertNotNull($postVote);
    }

    public function testGetSetIdReturnsId()
    {
        // Arrange
        $postVote = new PostVote();
        $postVote->setId(5);
        $expectedResult = 5;

        // Act
        $id = $postVote->getId();

        // Assert
        $this->assertEquals($expectedResult, $id);
    }

    public function testGetSetUserReturnsUser()
    {
        // Arrange
        $postVote = new PostVote();
        $postVote->setUser(2);
        $expectedResult = 2;

        // Act
        $user = $postVote->getUser();

        // Assert
        $this->assertEquals($expectedResult, $user);
    }

    public function testGetSetPostReturnsPost()
    {
        // Arrange
        $postVote = new PostVote();
        $postVote->setPost(15);
        $expectedResult = 15;

        // Act
        $post = $postVote->getPost();

        // Assert
        $this->assertEquals($expectedResult, $post);
    }

    public function testGetSetVoteReturnVote()
    {
        // Arrange
        $postVote = new PostVote();
        $postVote->setVote(1);
        $expectedResult = 1;

        // Act
        $vote = $postVote->getVote();

        // Assert
        $this->assertEquals($expectedResult, $vote);
    }
}
