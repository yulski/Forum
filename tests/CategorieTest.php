<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 19/04/16
 * Time: 12:06
 */

namespace ForumTest;

use Forum\Table\Categorie as Category;

class CategorieTest extends \PHPUnit_Framework_TestCase
{
    public function testCanCreateObject()
    {
        //Arrange
        $category = new Category();

        //Act

        // Assert
        $this->assertNotNull($category);
    }

    public function testSetIdReturnsId()
    {
        // Arrange
        $category = new Category();
        $category->setId(3);
        $expectedResult = 3;

        // Act
        $id = $category->getId();

        // Assert
        $this->assertEquals($expectedResult, $id);
    }

    public function testSetNameReturnsName()
    {
        // Arrange
        $category = new Category();
        $category->setName('testCategory');
        $expectedResult = 'testCategory';

        // Act
        $name = $category->getName();

        // Assert
        $this->assertEquals($expectedResult, $name);
    }

    public function testSetDescriptionReturnsDescription()
    {
        // Arrange
        $category = new Category();
        $category->setDescription('test category description');
        $expectedResult = 'test category description';

        // Act
        $description = $category->getDescription();

        // Assert
        $this->assertEquals($expectedResult, $description);
    }

    /**
     * @dataProvider categoryNameDataProvider
     */
    public function testValidateCategoryName($name, $expectedMessage, $expectException)
    {
        $exceptionThrown = false;
        $message = '';
        try {
            Category::validateCategoryName($name);
        } catch (\Exception $e) {
            $exceptionThrown = true;
            $message = $e->getMessage();
        } finally {
            if ($expectException) {
                $this->assertEquals($expectedMessage, $message);
            } else {
                $this->assertFalse($exceptionThrown);
            }
        }
    }

    /**
     * @dataProvider categoryDescriptionDataProvider
     */
    public function testValidateCategoryDescription($description, $expectedMessage, $expectException)
    {
        $exceptionThrown = false;
        $message = '';
        try {
            Category::validateCategoryDescription($description);
        } catch (\Exception $e) {
            $exceptionThrown = true;
            $message = $e->getMessage();
        } finally {
            if ($expectException) {
                $this->assertEquals($expectedMessage, $message);
            } else {
                $this->assertFalse($exceptionThrown);
            }
        }
    }

    public function categoryNameDataProvider()
    {
        return array(
            array('', 'Category name is empty.', true),
            array('a', 'Category name is too short.', true),
            array('test category name test category name test category name test category name test category name test category name test category name', 'Category name is too long.', true),
            array('valid category name', '', false)
        );
    }

    public function categoryDescriptionDataProvider()
    {
        return array(
            array('', 'Category description is empty.', true),
            array('a', 'Category description is too short.', true),
            array('test category description test category description test category description test category description test category description test category description test category description test category description test category description test category description test category description test category description test category description test category description test category description test category description test category description test category description test category description test category description test category description test category description', 'Category description is too long.', true),
            array('this is a valid category description', '', false)
        );
    }
}
