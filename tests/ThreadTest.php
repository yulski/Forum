<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 19/04/16
 * Time: 13:53
 */

namespace ForumTest;

use Forum\Table\Thread;

class ThreadTest extends \PHPUnit_Framework_TestCase
{

    public function testCanCreateNewObject()
    {
        // Arrange
        $thread = new Thread();

        // Act

        // Assert
        $this->assertNotNull($thread);
    }

    public function testGetSetIdReturnsId()
    {
        // Arrange
        $thread = new Thread();
        $thread->setId(10);
        $expectedResult = 10;

        // Act
        $id = $thread->getId();

        // Assert
        $this->assertEquals($expectedResult, $id);
    }

    public function testGetSetSubjectReturnsSubject()
    {
        // Arrange
        $thread = new Thread();
        $thread->setSubject('thread subject');
        $expectedResult = 'thread subject';

        // Act
        $subject = $thread->getSubject();

        // Assert
        $this->assertEquals($expectedResult, $subject);
    }

    public function testGetSetDateCreatedReturnsDateCreated()
    {
        // Arrange
        $thread = new Thread();
        $date = date('Y-m-d');
        $thread->setDateCreated($date);
        $expectedResult = $date;

        // Act
        $dateCreated = $thread->getDateCreated();

        // Assert
        $this->assertEquals($expectedResult, $dateCreated);
    }

    public function testGetSetCategoryReturnsCategory()
    {
        // Arrange
        $thread = new Thread();
        $thread->setCategory(3);
        $expectedResult = 3;

        // Act
        $category = $thread->getCategory();

        // Assert
        $this->assertEquals($expectedResult, $category);
    }

    public function testGetSetThreadByReturnsThreadBy()
    {
        // Arrange
        $thread = new Thread();
        $thread->setThreadBy(4);
        $expectedResult = 4;

        // Act
        $threadBy = $thread->getThreadBy();

        // Assert
        $this->assertEquals($expectedResult, $threadBy);
    }

    /**
     * @dataProvider threadSubjectDataProvider
     */
    public function testValidateThreadSubject($subject, $expectedMessage, $expectException)
    {
        $exceptionThrown = false;
        $message = '';

        try {
            Thread::validateThreadSubject($subject);
        } catch (\Exception $e) {
            $exceptionThrown = true;
            $message = $e->getMessage();
        } finally {
            if ($expectException) {
                $this->assertEquals($expectedMessage, $message);
            } else {
                $this->assertFalse($exceptionThrown);
            }
        }
    }

    public function threadSubjectDataProvider()
    {
        return array(
            array('', 'Thread subject is empty.', true),
            array('a', 'Thread subject is too short.', true),
            array('test thread subject test thread subject test thread subject test thread subject test thread subject test thread subject test thread subject test thread subject test thread subject test thread subject test thread subject test thread subject test thread subject test thread subject\';
        $expectedMessage = \'Thread subject is too long.', 'Thread subject is too long.', true),
            array('valid thread subject', '', false)
        );
    }
}
