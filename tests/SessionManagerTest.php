<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 20/04/16
 * Time: 15:14
 */

namespace ForumTest;

use Forum\Util\SessionManager;
use Forum\Util\Message;
use Forum\Table\User;
use Forum\Table\Post;
use Forum\Table\Thread;
use Forum\Table\Categorie as Category;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;

class SessionManagerTest extends \PHPUnit_Framework_TestCase
{

    public function getMockedSession()
    {
        $session = new Session(new MockArraySessionStorage());
        return $session;
    }

    /**
     * @dataProvider messageDataProvider
     */
    public function testGetSetMessage($messageContent, $messageClass)
    {
        // Arrange
        $session = $this->getMockedSession();
        SessionManager::setMessage($messageContent, $messageClass, $session);
        $expectedResult = array(
            'content' => $messageContent,
            'class' => $messageClass
        );

        // Act
        $message = SessionManager::getMessage($session);

        // Assert
        $this->assertEquals($expectedResult, $message);
    }

    /**
     * @dataProvider messageDataProvider
     */
    public function testClearMessage($messageContent, $messageClass)
    {
        // Arrange
        $session = $this->getMockedSession();
        SessionManager::setMessage($messageContent, $messageClass, $session);
        SessionManager::clearMessage($session);

        // Act
        $message = SessionManager::getMessage($session);

        // Assert
        $this->assertNull($message);
    }

    /**
     * @dataProvider userDataProvider
     */
    public function testGetSetCurrentUser($currentUser)
    {
        // Arrange
        $session = $this->getMockedSession();
        SessionManager::setCurrentUser($currentUser, $session);
        $expectedResult = $currentUser;

        // Act
        $user = SessionManager::getCurrentUser($session);

        // Assert
        $this->assertEquals($expectedResult, $user);
    }

    /**
     * @dataProvider userDataProvider
     */
    public function testClearCurrentUser($currentUser)
    {
        // Arrange
        $session = $this->getMockedSession();
        SessionManager::setCurrentUser($currentUser, $session);
        SessionManager::clearCurrentUser($session);

        // Act
        $user = SessionManager::getCurrentUser($session);

        // Assert
        $this->assertNull($user);
    }

    /**
     * @dataProvider pictureDataProvider
     */
    public function testGetProfilePicture($user, $picture, $expectedResult)
    {
        // Arrange
        $session = $this->getMockedSession();
        if ($user) {
            $user->setPicture($picture);
            SessionManager::setCurrentUser($user, $session);
        }

        // Act
        $userPicture = SessionManager::getProfilePicture($session);

        // Assert
        if ($user) {
            $this->assertEquals($expectedResult, $userPicture);
        } else {
            $this->assertEmpty($userPicture);
        }
    }

    public function testGetSetLoginRedirect()
    {
        // Arrange
        $session = $this->getMockedSession();
        SessionManager::setLoginRedirect('/categories', $session);
        $expectedResult = '/categories';

        // Act
        $loginRedirect = SessionManager::getLoginRedirect($session);

        // Assert
        $this->assertEquals($expectedResult, $loginRedirect);
    }

    public function testGetSetCategoryId()
    {
        // Arrange
        $session = $this->getMockedSession();
        SessionManager::setCategoryId(2, $session);
        $expectedResult = 2;

        // Act
        $categoryId = SessionManager::getCategoryId($session);

        // Assert
        $this->assertEquals($expectedResult, $categoryId);
    }

    public function testGetSetThreadId()
    {
        // Arrange
        $session = $this->getMockedSession();
        SessionManager::setThreadId(7, $session);
        $expectedResult = 7;

        // Act
        $threadId = SessionManager::getThreadId($session);

        // Assert
        $this->assertEquals($expectedResult, $threadId);
    }

    public function testGetSetPost()
    {
        // Arrange
        $postContent = 'this is a test post';
        $postDate = '2015-11-15 20:51:10';
        $postUser = 2;
        $session = $this->getMockedSession();
        SessionManager::setPost($postContent, $postDate, $postUser, $session);
        $expectedResult = array(
            'content' => $postContent,
            'date' => $postDate,
            'user' => $postUser
        );

        // Act
        $post = SessionManager::getPost($session);

        // Assert
        $this->assertEquals($expectedResult, $post);
    }

    public function testAddGetFormData()
    {
        // Arrange
        $session = $this->getMockedSession();
        SessionManager::addFormData('key1', 'value1', $session);
        SessionManager::addFormData('key2', 'value2', $session);
        SessionManager::addFormData('key3', 'value3', $session);
        $expectedResult = array(
            'key1' => 'value1',
            'key2' => 'value2',
            'key3' => 'value3'
        );

        // Act
        $formData = SessionManager::getFormData($session);

        // Assert
        $this->assertEquals($expectedResult, $formData);
    }

    /**
     * @dataProvider
     */
    public function testAddSessionArguments()
    {
        // Arrange
        $session = $this->getMockedSession();
        SessionManager::setMessage('message', Message::INFO, $session);
        SessionManager::setCategoryId(3, $session);
        SessionManager::setCurrentUser(new User(), $session);
        $expectedResult = array(
            'message' => array(
                'content' => 'message',
                'class' => Message::INFO
            ),
            'currentUser' => new User(),
            'formData' => null,
            'categoryId' => 3,
            'threadId' => null,
            'userProfilePicture' => User::PICTURE_DIRECTORY_PATH . User::DEFAULT_PICTURE,
            'post' => null
        );

        // Act
        $args = SessionManager::addSessionArguments($session);

        // Assert
        $this->assertEquals($expectedResult, $args);
    }

    public function messageDataProvider()
    {
        return array(
            array('hello', Message::INFO),
            array('', Message::ERROR),
            array('message', Message::SUCCESS)
        );
    }

    public function userDataProvider()
    {
        $user = new User();
        $user->setId(2);
        $user->setUsername('user_123');
        $user->setEmail('user@yahoo.com');
        $user->setPassword('abcde12345');
        $user->setDateJoined('2014-10-22 15:15:15');
        $user->setAdministrator(0);
        $user->setReputation(3);
        $user->setPicture('2picture.jpg');
        return array(
            array($user)
        );
    }

    public function pictureDataProvider()
    {
        $user = new User();
        return array(
            array($user, 'picture.jpg', '/pictures/picture.jpg'),
            array($user, 'image.png', '/pictures/image.png'),
            array(null, '', '/pictures/default.jpg')
        );
    }
}
