<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 19/04/16
 * Time: 14:49
 */

namespace ForumTest;

use Forum\Table\Vote;

class VoteTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @dataProvider voteDataProvider
     */
    public function testValidateVote($vote, $expectedMessage, $expectException)
    {
        $exceptionThrown = false;
        $message = '';

        try {
            Vote::validateVote($vote);
        } catch (\Exception $e) {
            $exceptionThrown = true;
            $message = $e->getMessage();
        } finally {
            if ($expectException) {
                $this->assertEquals($expectedMessage, $message);
            } else {
                $this->assertFalse($exceptionThrown);
            }
        }
    }

    public function voteDataProvider()
    {
        return array(
            array('a', 'Incorrect value for vote', true),
            array(7, 'Incorrect value for vote', true),
            array(1, '', false),
            array(0, '', false)
        );
    }
}
