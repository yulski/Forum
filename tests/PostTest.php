<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 19/04/16
 * Time: 13:19
 */

namespace ForumTest;

use Forum\Table\Post;
use PHPUnit_Extensions_Database_DataSet_IDataSet;
use PHPUnit_Extensions_Database_DB_IDatabaseConnection;

class PostTest extends \PHPUnit_Framework_TestCase
{

    public function testCanCreateNewObject()
    {
        // Arrange
        $post = new Post();

        // Act

        // Assert
        $this->assertNotNull($post);
    }

    public function testGetSetIdReturnsId()
    {
        // Arrange
        $post = new Post();
        $post->setId(9);
        $expectedResult = 9;

        // Act
        $id = $post->getId();

        // Assert
        $this->assertEquals($expectedResult, $id);
    }

    public function testGetSetPostContentReturnsPostContent()
    {
        // Arrange
        $post = new Post();
        $post->setPostContent('post content');
        $expectedResult = 'post content';

        // Act
        $postContent = $post->getPostContent();

        // Assert
        $this->assertEquals($expectedResult, $postContent);
    }

    public function testGetSetPostDateReturnsPostDate()
    {
        // Arrange
        $post = new Post();
        $time = date('Y-m-d');
        $post->setPostDate($time);
        $expectedResult = $time;

        //Act
        $postDate = $post->getPostDate();

        // Assert
        $this->assertEquals($expectedResult, $postDate);
    }

    public function testGetSetPostThreadReturnsPostThread()
    {
        // Arrange
        $post = new Post();
        $post->setPostThread(5);
        $expectedResult = 5;

        // Act
        $postThread = $post->getPostThread();

        // Assert
        $this->assertEquals($expectedResult, $postThread);
    }

    public function testGetSetPostByReturnsPostBy()
    {
        // Arrange
        $post = new Post();
        $post->setPostBy(17);
        $expectedResult = 17;

        // Act
        $postBy = $post->getPostBy();

        // Assert
        $this->assertEquals($expectedResult, $postBy);
    }
}
