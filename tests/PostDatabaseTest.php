<?php
/**
 * Created by PhpStorm.
 * User: julek
 * Date: 20/04/16
 * Time: 00:11
 */

namespace ForumTest;

use Mattsmithdev\PdoCrud\DatabaseTable;
use Forum\Table\ForumDBTable;
use Forum\Table\Post;

class PostDatabaseTest extends \PHPUnit_Extensions_Database_TestCase
{

    protected function getConnection()
    {
        $host = DB_HOST;
        $dbName = DB_NAME;
        $dbUser = DB_USER;
        $dbPass = DB_PASS;

        // mysql
        $dsn = 'mysql:host=' . $host . ';dbname=' . $dbName;
        $db = new \PDO($dsn, $dbUser, $dbPass);
        $connection = $this->createDefaultDBConnection($db, $dbName);

        return $connection;
    }

    protected function getDataSet()
    {
        $seedFilePath = __DIR__ . '/databaseXml/seed.xml';
        return $this->createXMLDataSet($seedFilePath);
    }

    /**
     * @dataProvider searchByColumnDataProvider
     */
    public function testSearchByColumn($column, $value, $expectedPost, $searchType)
    {
        // Arrange
        $expectedResult = array(
            $expectedPost
        );

        // Act
        $result = Post::searchByColumn($column, $value, $searchType);

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @dataProvider searchMultipleColumnsDataProvider
     */
    public function testSearchMultipleColumns($columnArray, $valueArray, $expectedPost, $searchType)
    {
        // Arrange
        $expectedResult = array(
            $expectedPost
        );

        // Act
        $result = Post::searchMultipleColumns($columnArray, $valueArray, $searchType);

        // Assert
        $this->assertEquals($expectedResult, $result);
    }

    public function test()
    {
        $this->assertEquals(true, true);
    }

    public function searchByColumnDataProvider()
    {
        $expectedPost = new Post();
        $expectedPost->setId(1);
        $expectedPost->setPostContent('This is a post');
        $expectedPost->setPostDate('2016-04-19 06:21:20');
        $expectedPost->setPostThread(1);
        $expectedPost->setPostBy(2);
        return array(
            array('postDate', '2016-04-19 06:21:20', $expectedPost, ForumDBTable::SEARCH_EQUAL),
            array('postDate', '2016-04-19 06:21:20', $expectedPost, ForumDBTable::SEARCH_LIKE),
            array('postDate', '2016-04-19 06:21:20', $expectedPost, 12345)
        );
    }

    public function searchMultipleColumnsDataProvider()
    {
        $expectedPost = new Post();
        $expectedPost->setId(1);
        $expectedPost->setPostContent('This is a post');
        $expectedPost->setPostDate('2016-04-19 06:21:20');
        $expectedPost->setPostThread(1);
        $expectedPost->setPostBy(2);

        return array(
            array(
                array('postDate', 'postThread'),
                array('2016-04-19 06:21:20', 1),
                $expectedPost,
                ForumDBTable::SEARCH_EQUAL
            ),
            array(
                array('postDate', 'postThread'),
                array('2016-04-19 06:21:20', 1),
                $expectedPost,
                ForumDBTable::SEARCH_LIKE
            ),
            array(
                array('postDate', 'postThread'),
                array('2016-04-19 06:21:20', 1),
                $expectedPost,
                1234
            )
        );
    }
}
