<?php

require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/db_config.php';

$path = __DIR__ . '/../templates';

// define path for user profile pictures
define('PICTURE_DIRECTORY_PATH', '/pictures/');
define('DEFAULT_PICTURE', 'default.jpg');

$app = new Silex\Application();

$app['debug'] = true; // TODO remove later

$app->register(new Silex\Provider\SessionServiceProvider());

$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => $path,
    'twig.options' => array(
        'strict_variables' => false
    )
));
