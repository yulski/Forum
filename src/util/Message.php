<?php

/**
 * namespace of utility classes
 */
namespace Forum\util;

/**
 * Class Message - holds constants for different types of messages.
 * @package Forum\Util
 */
final class Message
{
    // constants with bootstrap classes for different types of message
    const INFO = 'alert alert-info';
    const WARNING = 'alert alert-warning';
    const SUCCESS = 'alert alert-success';
    const ERROR = 'alert alert-danger';
}
