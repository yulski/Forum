<?php

/**
 * namespace of utility classes
 */
namespace Forum\util;

use Forum\Table\User;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Class SessionManager - manages session data.
 * @package Forum\Util
 */
class SessionManager
{

    /**
     * Returns the message stored in session
     * @param Session $session
     * @return mixed
     */
    public static function getMessage(Session $session)
    {
        return $session->get('message');
    }

    /**
     * Stores a message in session
     * @param $message
     * @param $messageClass
     * @param Session $session
     */
    public static function setMessage($message, $messageClass, Session $session)
    {
        // store message as array, with content and class keys
        $session->set('message', array(
            'content' => $message,
            'class' => $messageClass
        ));
    }

    /**
     * Removes the message stored in session
     * @param Session $session
     */
    public static function clearMessage(Session $session)
    {
        $session->set('message', null);
    }

    /**
     * Returns the current user stored in session
     * @param Session $session
     * @return User
     */
    public static function getCurrentUser(Session $session)
    {
        return $session->get('currentUser');
    }

    /**
     * Stores a user as current user in session
     * @param User $user
     * @param Session $session
     */
    public static function setCurrentUser(User $user, Session $session)
    {
        $session->set('currentUser', $user);
    }

    /**
     * Removes current user from session
     * @param Session $session
     */
    public static function clearCurrentUser(Session $session)
    {
        $session->set('currentUser', null);
    }

    /**
     * Returns the current user's profile picture, or default picture if user does not have a profile picture.
     * @param Session $session
     * @return string
     */
    public static function getProfilePicture(Session $session)
    {
        // get current user
        if ($user = self::getCurrentUser($session)) {
            // get the current user's picture
            $picture = $user->getPicture();
            if (!$picture) {
                // use default picture if user does not have a picture
                $picture = User::DEFAULT_PICTURE;
            }
            // return picture
            return User::PICTURE_DIRECTORY_PATH . $picture;
        }
        // return empty string if no user logged in
        return '';
    }

    /**
     * Returns the login redirect stored in session
     * @param Session $session
     * @return string
     */
    public static function getLoginRedirect(Session $session)
    {
        return $session->get('loginRedirect');
    }

    /**
     * Stores login redirect in session
     * @param $loginRedirect
     * @param Session $session
     */
    public static function setLoginRedirect($loginRedirect, Session $session)
    {
        $session->set('loginRedirect', $loginRedirect);
    }

    /**
     * Returns the category id stored in session
     * @param Session $session
     * @return int
     */
    public static function getCategoryId(Session $session)
    {
        return $session->get('categoryId');
    }

    /**
     * Stores a category id in session
     * @param $categoryId
     * @param Session $session
     */
    public static function setCategoryId($categoryId, Session $session)
    {
        $session->set('categoryId', $categoryId);
    }

    /**
     * Returns the thread id stored in session
     * @param Session $session
     * @return int
     */
    public static function getThreadId(Session $session)
    {
        return $session->get('threadId');
    }

    /**
     * Stores thread id in session
     * @param $threadId
     * @param Session $session
     */
    public static function setThreadId($threadId, Session $session)
    {
        $session->set('threadId', $threadId);
    }

    /**
     * Returns the post stored in session
     * @param Session $session
     * @return array
     */
    public static function getPost(Session $session)
    {
        return $session->get('post');
    }

    /**
     * Stores a post in session in the form of an array
     * @param $postContent
     * @param $postDate
     * @param $postUser
     * @param Session $session
     */
    public static function setPost($postContent, $postDate, $postUser, Session $session)
    {
        $session->set('post', array(
            'content' => $postContent,
            'date' => $postDate,
            'user' => $postUser
        ));
    }

    /**
     * Returns the form data array stored in session
     * @param Session $session
     * @return array
     */
    public static function getFormData(Session $session)
    {
        return $session->get('formData');
    }

    /**
     * Adds a key-value pair to form data array stored in session
     * @param $key
     * @param $value
     * @param Session $session
     */
    public static function addFormData($key, $value, Session $session)
    {
        $formData = $session->get('formData');
        $formData[$key] = $value;
        $session->set('formData', $formData);
    }

    /**
     * Adds data stored in session to an array and returns that array
     * @param Session $session
     * @param array $args
     * @return array
     */
    public static function addSessionArguments(Session $session, $args = array())
    {
        $args['message'] = self::getMessage($session);
        $args['currentUser'] = self::getCurrentUser($session);
        $args['formData'] = self::getFormData($session);
        $args['categoryId'] = self::getCategoryId($session);
        $args['threadId'] = self::getThreadId($session);
        $args['userProfilePicture'] = self::getProfilePicture($session);
        $args['post'] = self::getPost($session);
        return $args;
    }
}
