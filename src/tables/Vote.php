<?php

/**
 * namespace of classes related to database tables
 */
namespace Forum\Table;

/**
 * Class Vote - abstract class for vote tables.
 * @package Forum\Table
 */
abstract class Vote extends ForumDBTable
{
    /**
     * Validates a vote
     * @param int $vote
     * @throws \Exception
     */
    final public static function validateVote($vote)
    {
        // if vote isn't 0 or 1, throw exception
        if ($vote !== 0 && $vote !== 1) {
            throw new \Exception('Incorrect value for vote');
        }
    }
}
