<?php

/**
 * namespace of classes related to database tables
 */
namespace Forum\Table;

/**
 * Class PostVote - represents the postVotes table.
 * @package Forum\Table
 */
class PostVote extends Vote
{
    /**
     * @var int id of the vote
     */
    private $id;

    /**
     * @var int id of the user whom the vote belongs to
     */
    private $user;

    /**
     * @var int id of the post the vote is on
     */
    private $post;

    /**
     * @var int the vote itself (0 for down, 1 for up)
     */
    private $vote;

    /**
     * Returns id of vote
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets if of vote
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Returns the id of the user whom the vote belongs to
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Sets id of user associated with the vote
     * @param int $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * Returns id of the post the vote is on
     * @return int
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * Sets id of vote the post is on
     * @param int $post
     */
    public function setPost($post)
    {
        $this->post = $post;
    }

    /**
     * Returns the vote
     * @return int
     */
    public function getVote()
    {
        return $this->vote;
    }

    /**
     * Sets the vote
     * @param int $vote
     */
    public function setVote($vote)
    {
        $this->vote = $vote;
    }
}
