<?php

/**
 * namespace of classes related to database tables
 */
namespace Forum\Table;

/**
 * Class Post - represents Posts table.
 * @package Forum\Table
 */
class Post extends ForumDBTable
{
    /**
     * @var int post id
     */
    private $id;

    /**
     * @var string the main content of the post
     */
    private $postContent;

    /**
     * @var mixed date the post was posted
     */
    private $postDate;

    /**
     * @var int id of thread the post was posted in
     */
    private $postThread;

    /**
     * @var int id of user who posted the post
     */
    private $postBy;

    /**
     * Returns the post id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the post id
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Returns the post content
     * @return string
     */
    public function getPostContent()
    {
        return $this->postContent;
    }

    /**
     * Sets the post content
     * @param string $postContent
     */
    public function setPostContent($postContent)
    {
        $this->postContent = $postContent;
    }

    /**
     * Returns post data
     * @return mixed
     */
    public function getPostDate()
    {
        return $this->postDate;
    }

    /**
     * Sets post date
     * @param mixed $postDate
     */
    public function setPostDate($postDate)
    {
        $this->postDate = $postDate;
    }

    /**
     * Returns id of the thread the post belongs to
     * @return int
     */
    public function getPostThread()
    {
        return $this->postThread;
    }

    /**
     * Sets the id of the thread the post belongs to
     * @param int $postThread
     */
    public function setPostThread($postThread)
    {
        $this->postThread = $postThread;
    }

    /**
     * Returns id of user who posted the post
     * @return int
     */
    public function getPostBy()
    {
        return $this->postBy;
    }

    /**
     * Sets the id of user who posted the post
     * @param int $postBy
     */
    public function setPostBy($postBy)
    {
        $this->postBy = $postBy;
    }
}
