<?php

/**
 * namespace of classes related to database tables
 */
namespace Forum\Table;

/**
 * Class User - represents Users table
 * @package Forum\Table
 */
class User extends ForumDBTable
{

    const PICTURE_DIRECTORY_PATH = '/pictures/';
    const DEFAULT_PICTURE = 'default.jpg';

    /**
     * @var int user's id
     */
    private $id;

    /**
     * @var string user's username
     */
    private $username;

    /**
     * @var string user's email address
     */
    private $email;

    /**
     * @var string user's password
     */
    private $password;


    /**
     * @var mixed the date the user registered
     */
    private $dateJoined;

    /**
     * @var int if the user is an admin or not
     */
    private $administrator;

    /**
     * @var int user's reputation score
     */
    private $reputation;

    /**
     * @var string name of user's profile picture
     */
    private $picture;

    // constants for max and min length of username, email and password
    const USERNAME_MAX_LENGTH = 32;
    const USERNAME_MIN_LENGTH = 5;
    const EMAIL_MAX_LENGTH = 256;
    const EMAIL_MIN_LENGTH = 6;
    const PASSWORD_MAX_LENGTH = 256;
    const PASSWORD_MIN_LENGTH = 8;

    /**
     * Returns the name of the user's profile picture
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Sets the name of the user's profile picture
     * @param string $picture
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    }

    /**
     * Returns the user id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the user id
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Returns the user's username
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Sets the user's username
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * Returns the user's email
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the user's email
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Returns the user's password
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Sets the user's password
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Returns the date the user registered
     * @return mixed
     */
    public function getDateJoined()
    {
        return $this->dateJoined;
    }

    /**
     * Sets the date the user registered
     * @param mixed $dateJoined
     */
    public function setDateJoined($dateJoined)
    {
        $this->dateJoined = $dateJoined;
    }

    /**
     * Returns whether the user is an administrator or not
     * @return int
     */
    public function getAdministrator()
    {
        return $this->administrator;
    }

    /**
     * Sets whether the user is an administrator or not
     * @param int $administrator
     */
    public function setAdministrator($administrator)
    {
        $this->administrator = $administrator;
    }

    /**
     * Returns the user's reputation score
     * @return int
     */
    public function getReputation()
    {
        return $this->reputation;
    }

    /**
     * Sets the user's reputation score
     * @param int $reputation
     */
    public function setReputation($reputation)
    {
        $this->reputation = $reputation;
    }

    /**
     * Adds one to the user's reputation
     */
    public function addReputation()
    {
        // get current reputation, add one and set that as new reputation
        $reputation = $this->getReputation();
        $reputation++;
        $this->setReputation($reputation);
    }

    /**
     * Subtracts one from the user's reputation
     */
    public function loseReputation()
    {
        // get current reputation, subtract one and set that as new reputation
        $reputation = $this->getReputation();
        $reputation--;
        $this->setReputation($reputation);
    }

    /**
     * Validates a username
     * @param string $username
     * @throws \Exception
     */
    public static function validateUsername($username)
    {
        // throw exception if username empty
        if (empty($username)) {
            throw new \Exception('Username is empty');
        } else {
            $length = strlen($username);
            // throw exception if username too long or short
            if ($length > self::USERNAME_MAX_LENGTH) {
                throw new \Exception('Username is too long');
            } elseif ($length < self::USERNAME_MIN_LENGTH) {
                throw new \Exception('Username is too short');
            }
        }
    }

    /**
     * Validate an email address
     * @param string $email
     * @throws \Exception
     */
    public static function validateEmail($email)
    {
        // if email empty, throw exception
        if (empty($email)) {
            throw new \Exception('Email is empty');
        } elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // if the email address is not a valid email address, throw exception
            throw new \Exception('Email is not in valid email format.');
        } elseif (strlen($email) < self::EMAIL_MIN_LENGTH) {
            throw new \Exception('Email is too short');
        }
    }

    /**
     * Validates a password
     * @param string $password
     * @throws \Exception
     */
    public static function validatePassword($password)
    {
        // if password empty, throw exception
        if (empty($password)) {
            throw new \Exception('Password is empty');
        } else {
            $length = strlen($password);
            // if password too long or too short, throw exception
            if ($length > self::PASSWORD_MAX_LENGTH) {
                throw new \Exception('Password is too long');
            } elseif ($length < self::PASSWORD_MIN_LENGTH) {
                throw new \Exception('Password is too short');
            }
        }
    }
}
