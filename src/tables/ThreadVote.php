<?php

/**
 * namespace of classes related to database tables
 */
namespace Forum\Table;

/**
 * Class ThreadVote - represents threadVotes table.
 * @package Forum\Table
 */
class ThreadVote extends Vote
{
    /**
     * @var int id of the vote
     */
    private $id;

    /**
     * @var int id of the thread the vote is on
     */
    private $thread;

    /**
     * @var int id of the user who voted
     */
    private $user;

    /**
     * @var int the vote itself (0 for down, 1 for up)
     */
    private $vote;

    /**
     * Returns the vote id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the vote id
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Returns id of thread the vote is on
     * @return int
     */
    public function getThread()
    {
        return $this->thread;
    }

    /**
     * Sets id of thread vote is on
     * @param int $thread
     */
    public function setThread($thread)
    {
        $this->thread = $thread;
    }

    /**
     * Returns id of user who voted
     * @return int
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Sets id of user who voted
     * @param int $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * Returns the vote
     * @return int
     */
    public function getVote()
    {
        return $this->vote;
    }

    /**
     * Sets the vote
     * @param int $vote
     */
    public function setVote($vote)
    {
        $this->vote = $vote;
    }
}
