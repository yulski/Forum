<?php

/**
 * namespace of classes related to database tables
 */
namespace Forum\Table;

/**
 * Class Thread - represents Threads table.
 * @package Forum\Table
 */
class Thread extends ForumDBTable
{
    /**
     * @var int thread if
     */
    private $id;

    /**
     * @var string thread subject
     */
    private $subject;

    /**
     * @var mixed date the thread was created on
     */
    private $dateCreated;

    /**
     * @var int id of category the thread belongs to
     */
    private $category;

    /**
     * @var int id of user who posted the thread
     */
    private $threadBy;

    // constants for thread subject max and min allowed lengths
    const THREAD_SUBJECT_MAX_LENGTH = 256;
    const THREAD_SUBJECT_MIN_LENGTH = 8;

    /**
     * Returns the thread id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the thread id
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Returns the thread subject
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Sets the thread subject
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * Returns the date the thread was created
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Sets the thread creation date
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * Returns the id of the category the thread belongs to
     * @return int
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Sets the id of the category the thread belongs to
     * @param int $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * Returns the id of the user who posted the thread
     * @return int
     */
    public function getThreadBy()
    {
        return $this->threadBy;
    }

    /**
     * Sets the id of the user who posted the thread
     * @param int $threadBy
     */
    public function setThreadBy($threadBy)
    {
        $this->threadBy = $threadBy;
    }

    /**
     * Validates a thread subject
     * @param string $subject
     * @throws \Exception
     */
    public static function validateThreadSubject($subject)
    {
        // if thread subject empty, throw exception
        if (empty($subject)) {
            throw new \Exception('Thread subject is empty.');
        } else {
            $length = strlen($subject);
            // if thread subject too long or short, throw exception
            if ($length > self::THREAD_SUBJECT_MAX_LENGTH) {
                throw new \Exception('Thread subject is too long.');
            } elseif ($length < self::THREAD_SUBJECT_MIN_LENGTH) {
                throw new \Exception('Thread subject is too short.');
            }
        }
    }
}
