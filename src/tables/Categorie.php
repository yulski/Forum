<?php

/**
 * namespace of classes related to database tables
 */
namespace Forum\Table;

/**
 * Class Categorie - represents Categories table.
 * @package Forum\Table
 */
class Categorie extends ForumDBTable
{
    /**
     * @var int category id
     */
    private $id;

    /**
     * @var string category name
     */
    private $name;

    /**
     * @var string category description
     */
    private $description;

    // max and min lengths for fields
    const CATEGORY_NAME_MAX_LENGTH = 128;
    const CATEGORY_DESCRIPTION_MAX_LENGTH = 512;
    const CATEGORY_NAME_MIN_LENGTH = 6;
    const CATEGORY_DESCRIPTION_MIN_LENGTH = 16;

    /**
     * Returns the category id
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Sets the category id
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Returns the category name
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the category name
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the category description
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Sets the category description
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Validates a category name
     * @param string $name
     * @throws \Exception
     */
    public static function validateCategoryName($name)
    {
        // if name empty, throw exception
        if (empty($name)) {
            throw new \Exception('Category name is empty.');
        } else {
            // if name length more than max allowed or less than min allowed, throw exception
            $length = strlen($name);
            if ($length > self::CATEGORY_NAME_MAX_LENGTH) {
                throw new \Exception('Category name is too long.');
            } elseif ($length < self::CATEGORY_NAME_MIN_LENGTH) {
                throw new \Exception('Category name is too short.');
            }
        }
    }

    /**
     * Validates a category description
     * @param string $description
     * @throws \Exception
     */
    public static function validateCategoryDescription($description)
    {
        // if description empty, throw exception
        if (empty($description)) {
            throw new \Exception('Category description is empty.');
        } else {
            // if description length more than max allowed or less than min allowed, throw exception
            $length = strlen($description);
            if ($length > self::CATEGORY_DESCRIPTION_MAX_LENGTH) {
                throw new \Exception('Category description is too long.');
            } elseif ($length < self::CATEGORY_DESCRIPTION_MIN_LENGTH) {
                throw new \Exception('Category description is too short.');
            }
        }
    }
}
