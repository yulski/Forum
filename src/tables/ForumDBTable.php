<?php

/**
 * namespace of classes related to database tables
 */
namespace Forum\Table;

use Mattsmithdev\PdoCrud\DatabaseTable;
use Mattsmithdev\PdoCrud\DatabaseManager;

/**
 * Class ForumDBTable - Abstraction of a database table inheriting from Mattsmithdev\PdoCrud\DatabaseTable and allowing for operations to be performed on database table.
 * @package Forum\Table
 */
class ForumDBTable extends DatabaseTable
{
    // constants for search accuracy
    const SEARCH_EQUAL = 0;
    const SEARCH_LIKE = 1;

    /**
     * Searches a column in the table for a particular value. Search can either use 'LIKE' or '=' for comparison based on $searchType parameter.
     * @param string $columnName
     * @param string $searchText
     * @param int $searchType
     * @return array
     */
    public static function searchByColumn($columnName, $searchText, $searchType = self::SEARCH_EQUAL)
    {
        // make sure search type is valid
        if ($searchType !== self::SEARCH_EQUAL && $searchType !== self::SEARCH_LIKE) {
            // use SEARCH_EQUAL by default
            $searchType = self::SEARCH_EQUAL;
        }

        // connect to database
        $db = new DatabaseManager();
        $connection = $db->getDbh();

        if ($searchType === self::SEARCH_LIKE) {
            $searchText = '%' . $searchText . '%';
        }

        // comparer is based on searchType
        $comparer = ($searchType === self::SEARCH_LIKE) ? ' LIKE ' : ' = ';

        // prepare statement
        $statement = $connection->prepare('SELECT * from ' . static::getTableName()  . ' WHERE ' . $columnName . $comparer .  ':searchText');
        $statement->bindParam(':searchText', $searchText, \PDO::PARAM_STR);
        // fetch class with this class name (aimed at inheritance)
        $statement->setFetchMode(\PDO::FETCH_CLASS, '\\' .  static::class);
        // exevute statement
        $statement->execute();

        $objects = $statement->fetchAll();
        // return result of query
        return $objects;
    }

    /**
     * Searches for a row where an array of values corresponds to the array of columns. Search can either use 'LIKE' or '=' for comparison based on $searchType parameter.
     * @param array $columnArray
     * @param array $valueArray
     * @param int $searchType
     * @return array
     */
    public static function searchMultipleColumns($columnArray, $valueArray, $searchType = self::SEARCH_EQUAL)
    {
        // make sure valid search type is used
        if ($searchType !== self::SEARCH_EQUAL && $searchType !== self::SEARCH_LIKE) {
            // use SEARCH_EQUAL by default
            $searchType = self::SEARCH_EQUAL;
        }

        // connect to database
        $db = new DatabaseManager();
        $connection = $db->getDbh();

        // comparer is based on search type
        $comparer = ($searchType === self::SEARCH_LIKE) ? ' LIKE ' : ' = ';

        // create query string
        $sql = 'SELECT * FROM ' . static::getTableName() . ' WHERE ';

        // loop through columns
        for ($i = 0; $i < count($columnArray); $i++) {
            // append column, comparer and valud to query string
            $sql .= $columnArray[$i] . $comparer . '\'' . $valueArray[$i] . '\'';
            // if not last iteration, append ' AND ' to query string
            if ($i <= count($valueArray) - 2) {
                $sql .= ' AND ';
            }
        }

        // prepare statement using query string
        $statement = $connection->prepare($sql);
        // statement will fetch class with this class name
        $statement->setFetchMode(\PDO::FETCH_CLASS, '\\' . static::class);
        $statement->execute();

        // return results
        return $statement->fetchAll();
    }
}
