<?php

/**
 * namespace of controller classes
 */
namespace Forum\Controller;

use Forum\Table\ForumDBTable;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Forum\Table\Thread;
use Forum\Table\Categorie as Category;
use Forum\Table\Post;
use Forum\Util\SessionManager;

/**
 * Class MainController - displays the basic pages without any special features.
 * @package Forum\Controller
 */
class MainController
{

    /**
     * Displays the registration form.
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function registerAction(Request $request, Application $app)
    {
        // put nav class in $args
        $args = array(
            'navRegisterClass' => 'active'
        );
        // add session args to $args array
        $args = SessionManager::addSessionArguments($app['session'], $args);
        $template = 'register.html.twig';

        // render template, clear message and return
        $output = $app['twig']->render($template, $args);
        SessionManager::clearMessage($app['session']);
        return $output;
    }

    /**
     * Displays the login form.
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function loginAction(Request $request, Application $app)
    {
        // put login nav class in $args
        $args = array(
            'navLoginClass' => 'active'
        );

        // get referer and save it in session
        $referer = $request->headers->get('referer');
        if (!$referer) {
            $referer = '/';
        }
        SessionManager::setLoginRedirect($referer, $app['session']);

        // add session args to $args array
        $args = SessionManager::addSessionArguments($app['session'], $args);
        $template = 'login.html.twig';

        // render template, clear message and return
        $output = $app['twig']->render($template, $args);
        SessionManager::clearMessage($app['session']);
        return $output;
    }

    /**
     * Displays the list of forum categories.
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function categoriesAction(Request $request, Application $app)
    {
        // set nav class for categories in $args
        $args = array(
            'navCategoriesClass' => 'active'
        );
        // add session args to $args array
        $args = SessionManager::addSessionArguments($app['session'], $args);
        $template = 'categories.html.twig';

        // get all categories and store them in $args array
        $categories = Category::getAll();
        $args['categories'] = $categories;

        // render template, clear message and return
        $output = $app['twig']->render($template, $args);
        SessionManager::clearMessage($app['session']);
        return $output;
    }

    /**
     * Displays the home page.
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function indexAction(Request $request, Application $app)
    {
        // set nav class for home page in $args
        $args = array(
            'navHomeClass' => 'active'
        );
        // add session args to $args array
        $args = SessionManager::addSessionArguments($app['session'], $args);
        $template = 'index.html.twig';

        // render template, clear message and return
        $output = $app['twig']->render($template, $args);
        SessionManager::clearMessage($app['session']);
        return $output;
    }

    /**
     * Processes search and displays the results.
     * @param Request $request
     * @param Application $app
     * @return array|string
     */
    public function processSearchAction(Request $request, Application $app)
    {
        $template = 'searchResults.html.twig';
        // get the search term submitted
        $searchTerm = addslashes($request->get('searchTerm'));

        // make sure that the search term is not empty
        if (empty($searchTerm)) {
            return $request->headers->get('referer');
        }

        // store the search term in session
        SessionManager::addFormData('searchTerm', $searchTerm, $app['session']);

        // get session args
        $args = SessionManager::addSessionArguments($app['session']);

        // search category names and descriptions for the search term
        $categoryNameResults = Category::searchByColumn('name', $searchTerm, ForumDBTable::SEARCH_LIKE);
        $categoryDescriptionResults = Category::searchByColumn('description', $searchTerm, ForumDBTable::SEARCH_LIKE);
        $categoryResults = array_merge($categoryNameResults, $categoryDescriptionResults);
        $categoryResults = array_unique($categoryResults, SORT_REGULAR);

        // search thread subjects
        $threadResults = Thread::searchByColumn('subject', $searchTerm, ForumDBTable::SEARCH_LIKE);

        // search post content
        $postResults = Post::searchByColumn('postContent', $searchTerm, ForumDBTable::SEARCH_LIKE);

        // store all results of search in $args
        $args['categoryResults'] = $categoryResults;
        $args['threadResults'] = $threadResults;
        $args['postResults'] = $postResults;

        // render template, clear message and return
        $output = $app['twig']->render($template, $args);
        SessionManager::clearMessage($app['session']);
        return $output;
    }
}
