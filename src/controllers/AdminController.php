<?php

/**
 * namespace of controller classes
 */
namespace Forum\Controller;

use Forum\Table\ForumDBTable;
use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use Forum\Table\Categorie as Category;
use Forum\Table\User;
use Forum\Util\SessionManager;
use Forum\Util\Message;

/**
 * Class AdminController - controller for actions that are exclusive to the administrator.
 * @package Forum\Controller
 */
class AdminController
{

    /**
     * Displays form creation form.
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function createCategoryAction(Request $request, Application $app)
    {
        // get session arguments
        $args = SessionManager::addSessionArguments($app['session']);
        $template = 'createCategory.html.twig';
        // render template and clear message
        $output = $app['twig']->render($template, $args);
        SessionManager::clearMessage($app['session']);
        // output rendered template
        return $output;
    }

    /**
     * Processes category creation and if all data is correct, creates a new category.
     * @param Request $request
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processCreateCategoryAction(Request $request, Application $app)
    {
        // get session arguments
        $args = SessionManager::addSessionArguments($app['session']);

        // get submitted category name and description
        $catName = addslashes($request->get('catName'));
        $catDesc = addslashes($request->get('catDescription'));

        // save form data for sticky form
        SessionManager::addFormData('createCategoryName', $catName, $app['session']);
        SessionManager::addFormData('createCategoryDescription', $catDesc, $app['session']);

        // validate submitted category name and description
        try {
            Category::validateCategoryName($catName);
            Category::validateCategoryDescription($catDesc);
        } catch (\Exception $e) {
            // if submitted data is not valid, set message and redirect
            $message = 'Error! ' . $e->getMessage();
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/createCategory');
        }

        // save category name and description in $args array
        $args['catName'] = $catName;
        $args['catDescription'] = $catDesc;

        // search for a category with the passed name
        $nameResults = Category::searchByColumn('name', $catName, ForumDBTable::SEARCH_LIKE);

        // if a category with that name exists, set message and redirect
        if (count($nameResults) !== 0) {
            $message = 'Error! A category with that name already exists';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/createCategory');
        }

        // create a new category with that name and description
        $category = new Category();
        $category->setName($catName);
        $category->setDescription($catDesc);
        // try to insert the new category into the database
        $insertResult = Category::insert($category);

        // if insertion failed, set message and redirect
        if (!$insertResult) {
            $message = 'Error! An database error has occurred.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/createCategory');
        }

        // remove form data from sticky form as it's no longer needed
        SessionManager::addFormData('createCategoryName', null, $app['session']);
        SessionManager::addFormData('createCategoryDescription', null, $app['session']);

        // show success message and return to /categories page
        $message = 'Category successfully created';
        SessionManager::setMessage($message, Message::SUCCESS, $app['session']);
        return $app->redirect('/categories');
    }

    /**
     * Displays a category edition form.
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function editCategoryAction(Request $request, Application $app)
    {
        // get session args and template
        $args = SessionManager::addSessionArguments($app['session']);
        $template = 'editCategory.html.twig';

        // get submitted category id
        if (!$categoryId = addslashes($request->get('categoryId'))) {
            $categoryId = SessionManager::getCategoryId($app['session']);
        }
        $category = Category::getOneById($categoryId);
        $categoryName = $category->getName();

        // save category name in $args and in session
        $args['categoryName'] = $categoryName;
        SessionManager::setCategoryId($categoryId, $app['session']);

        // get the category description and store in in $args
        $description = $category->getDescription();
        $args['categoryDescription'] = $description;

        $args['referer'] = '/categories';

        // render template, clear message and return rendered template
        $output = $app['twig']->render($template, $args);
        SessionManager::clearMessage($app['session']);
        return $output;
    }

    /**
     * Processes category edition and edits category accordingly if all data is valid.
     * @param Request $request
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processEditCategoryAction(Request $request, Application $app)
    {
        // get the new name, description and password submitted
        $newName = addslashes($request->get('newName'));
        $newDescription = addslashes($request->get('newDescription'));
        $password = addslashes($request->get('password'));

        // save them in form data for sticky form
        SessionManager::addFormData('editCategoryName', $newName, $app['session']);
        SessionManager::addFormData('editCategoryDescription', $newDescription, $app['session']);
        SessionManager::addFormData('editCategoryPassword', $password, $app['session']);

        // try to validate password
        try {
            User::validatePassword($password);
        } catch (\Exception $e) {
            // if validation fails, set message and redirect
            $message = 'Error! ' . $e->getMessage();
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/editCategory');
        }

        // get the currently logged in user
        $user = SessionManager::getCurrentUser($app['session']);
        // get the current user's password
        $userPassword = $user->getPassword();

        if (!password_verify($password, $userPassword)) {
            $message = 'Incorrect password.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/editCategory');
        }

        // get category id from session
        $categoryId = SessionManager::getCategoryId($app['session']);

        // validate the submitted category name and description
        try {
            Category::validateCategoryName($newName);
            Category::validateCategoryDescription($newDescription);
        } catch (\Exception $e) {
            // if validation fails, set message and redirect
            $message = 'Error! ' . $e->getMessage();
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/editCategory');
        }

        // get category from category id
        $category = Category::getOneById($categoryId);
        // set it's name and description to those submitted in the form and update it
        $category->setName($newName);
        $category->setDescription($newDescription);
        $updateResult = Category::update($category);

        // if update fails, set message and redirect
        if (!$updateResult) {
            $message = 'Error occurred when attempting to update the category name.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('deleteCategory');
        }

        // clear form date no longer needed for sticky form
        SessionManager::addFormData('editCategoryName', null, $app['session']);
        SessionManager::addFormData('editCategoryPassword', null, $app['session']);

        // show success message and redirect
        $message = 'Category successfully updated.';
        SessionManager::setMessage($message, Message::SUCCESS, $app['session']);
        return $app->redirect('/categories');
    }

    /**
     * Displays the page with form for deleting a category.
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function deleteCategoryAction(Request $request, Application $app)
    {
        // get session args and template
        $args = SessionManager::addSessionArguments($app['session']);
        $template = 'deleteCategory.html.twig';

        // get submitted category name
        if (!$categoryId = addslashes($request->get('categoryId'))) {
            $categoryId = SessionManager::getCategoryId($app['session']);
        }
        $category = Category::getOneById($categoryId);
        $categoryName = $category->getName();

        // save category id in $args and in session
        $args['categoryName'] = $categoryName;
        SessionManager::setCategoryId($categoryId, $app['session']);

        // save referer in $args
        $referer = '/categories';
        $args['referer'] = $referer;

        // render template and return it
        $output = $app['twig']->render($template, $args);
        SessionManager::clearMessage($app['session']);
        return $output;
    }

    /**
     * Processes category deletion and if all data is valid, deletes a category.
     * @param Request $request
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processDeleteCategoryAction(Request $request, Application $app)
    {
        // get the submitted password
        $password = addslashes($request->get('password'));
        SessionManager::addFormData('deleteCategoryPassword', $password, $app['session']);

        // try to validate password
        try {
            User::validatePassword($password);
        } catch (\Exception $e) {
            // if validation fails, set message and redirect
            $message = 'Error! ' . $e->getMessage();
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/deleteCategory');
        }

        // get the currently logged in user
        $user = SessionManager::getCurrentUser($app['session']);
        $userPassword = $user->getPassword();
        // verify the submitted password
        if (!password_verify($password, $userPassword)) {
            $message = 'Incorrect password.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/deleteCategory');
        }

        // get category id from session
        $categoryId = SessionManager::getCategoryId($app['session']);

        // try to delete category
        $deleteResult = Category::delete($categoryId);

        // if deletion fails, show message and return
        if (!$deleteResult) {
            $message = 'Error occurred when attempting to delete the category.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/deleteCategory');
        }

        // clear form date no longer needed for sticky form
        SessionManager::addFormData('deleteCategoryPassword', null, $app['session']);

        // show success message and redirect
        $message = 'Category successfully deleted.';
        SessionManager::setMessage($message, Message::SUCCESS, $app['session']);
        return $app->redirect('/categories');
    }
}
