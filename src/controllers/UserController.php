<?php

/**
 * namespace of controller classes
 */
namespace Forum\Controller;

use Symfony\Component\HttpFoundation\Request;
use Silex\Application;
use Forum\Table\Categorie as Category;
use Forum\Table\User;
use Forum\Table\Thread;
use Forum\Table\Post;
use Forum\Table\PostVote;
use Forum\Table\ThreadVote;
use Forum\Util\SessionManager;
use Forum\Util\Message;

/**
 * Class UserController - controls actions related to user accounts/profiles.
 * @package Forum\Controller
 */
class UserController
{

    /**
     * Processes registration and if all data is valid registers the new user.
     * @param Request $request
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processRegistrationAction(Request $request, Application $app)
    {
        $referer = $request->headers->get('referer');

        // get all submitted form data and store it in session
        $username = addslashes($request->get('username'));
        $email = addslashes($request->get('email'));
        $password1 = addslashes($request->get('password1'));
        $password2 = addslashes($request->get('password2'));

        SessionManager::addFormData('registrationUsername', $username, $app['session']);
        SessionManager::addFormData('registrationEmail', $email, $app['session']);
        SessionManager::addFormData('registrationPassword1', $password1, $app['session']);
        SessionManager::addFormData('registrationPassword2', $password2, $app['session']);

        // try to validate all submitted data
        try {
            User::validatePassword($password1);
            User::validatePassword($password2);
            User::validateEmail($email);
            User::validateUsername($username);
        } catch (\Exception $e) {
            // if validation fails, set error message and redirect
            $message = 'Error! ' . $e->getMessage();
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // if the two passwords don't match, set error message and redirect
        if (strcmp($password1, $password2) !== 0) {
            $message = 'Error! Your passwords do not match.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        $password = password_hash($password1, PASSWORD_BCRYPT);

        // make sure a user with the entered username isn't already registered
        $usernameResults = User::searchByColumn('username', $username);
        if (count($usernameResults) !== 0) {
            $message = 'Error! A user with that username already exists.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // make sure a user with the entered email address isn't already registered
        $emailResults = User::searchByColumn('email', $email);
        if (count($emailResults) !== 0) {
            $message = 'Error! A user with that email is already registered.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        $date = date('Y-m-d');

        // create the new user
        $user = new User();
        $user->setUsername($username);
        $user->setEmail($email);
        $user->setPassword($password);
        $user->setDateJoined($date);
        $user->setReputation(0);
        $user->setAdministrator(0);

        // try to insert the new user into database
        $insertResult = User::insert($user);

        // if insertion failed, set error message and redirect
        if (!$insertResult) {
            $message = 'Error! An database error has occurred.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // clear form data from session
        SessionManager::addFormData('registrationUsername', null, $app['session']);
        SessionManager::addFormData('registrationEmail', null, $app['session']);
        SessionManager::addFormData('registrationPassword1', null, $app['session']);
        SessionManager::addFormData('registrationPassword2', null, $app['session']);

        // set success message and redirect
        $message = 'You have successfully registered. Now you can log in';
        SessionManager::setMessage($message, Message::SUCCESS, $app['session']);
        return $app->redirect($referer);
    }

    /**
     * Processes user login and if data is correct, logs the user in.
     * @param Request $request
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processLoginAction(Request $request, Application $app)
    {
        $referer = $request->headers->get('referer');

        // get the submitted username and password
        $username = addslashes($request->get('username'));
        $password = addslashes($request->get('password'));
        // store username and password in session
        SessionManager::addFormData('loginUsername', $username, $app['session']);
        SessionManager::addFormData('loginPassword', $password, $app['session']);

        // try to validate username and password
        try {
            User::validatePassword($password);
            User::validateUsername($username);
        } catch (\Exception $e) {
            // if validation fails, set error message and redirect
            $message = 'Error! ' . $e->getMessage();
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // find a user with the username entered
        $user = User::searchByColumn('username', $username);
        if (!$user) {
            $message = 'Error! A user with that username was not found.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        $user = $user[0];
        $userPassword = $user->getPassword();

        if (!password_verify($password, $userPassword)) {
            $message = 'Error! Incorrect password.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // store the user in session
        SessionManager::setCurrentUser($user, $app['session']);

        // clear form data
        SessionManager::addFormData('loginUsername', null, $app['session']);
        SessionManager::addFormData('loginPassword', null, $app['session']);

        // set success message and redirect to the login redirect stored in session
        $message = 'Logged in successfully.';
        SessionManager::setMessage($message, Message::INFO, $app['session']);
        $redirect = SessionManager::getLoginRedirect($app['session']);
        SessionManager::setLoginRedirect(null, $app['session']);

        return $app->redirect($redirect);
    }

    /**
     * Logs the user out.
     * @param Request $request
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function logoutAction(Request $request, Application $app)
    {
        $referer = $request->headers->get('referer');
        // clear current user from session
        SessionManager::clearCurrentUser($app['session']);
        // set message to confirm logout and redirect
        $message = 'Logged out';
        SessionManager::setMessage($message, Message::INFO, $app['session']);
        return $app->redirect($referer);
    }

    /**
     * Displays the user's profile page.
     * @param Request $request
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function userProfileAction(Request $request, Application $app)
    {
        // get session args and template
        $args = SessionManager::addSessionArguments($app['session']);
        $template = 'profile.html.twig';

        // if no user logged in, set error message and redirect to login page
        if (!SessionManager::getCurrentUser($app['session'])) {
            $message = 'You must be logged in to view your profile.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/login');
        }

        // get the logged in user and all the threads and posts created by that user
        $user = SessionManager::getCurrentUser($app['session']);
        $userId = $user->getId();
        $threads = Thread::searchByColumn('threadBy', $userId);
        $posts = Post::searchByColumn('postBy', $userId);
        $args['threads'] = array();
        $args['posts'] = array();

        // loop through the threads the user has created
        foreach ($threads as $thread) {
            // $arr is an array representing a single thread
            $arr = array();
            // create a link to the category the thread is in
            $link = '/categories';
            $categoryName = str_replace(' ', '%20', Category::getOneById($thread->getCategory())->getName());
            $link .= '/' . $categoryName;
            $arr['link'] = $link;
            // stire the data and subject of the thread in $arr
            $arr['subject'] = $thread->getSubject();
            $arr['date'] = $thread->getDateCreated();
            // add $arr as another thread in $args
            $args['threads'][] = $arr;
        }

        // loop through all the posts the user has created
        foreach ($posts as $post) {
            // get data associated with that post
            $link = '/categories';
            $thread = Thread::getOneById($post->getPostThread());
            $threadName = str_replace(' ', '%20', $thread->getSubject());
            $category = Category::getOneById($thread->getCategory());
            $categoryName = $category->getName();

            // create a link to the thread that post is in
            $link .= '/' . $categoryName . '/' . $threadName;
            $arr = array();
            $arr['link'] = $link;
            // store post data in $arr
            $arr['content'] = $post->getPostContent();
            $arr['date'] = $post->getPostDate();
            // add $arr as another post in $args
            $args['posts'][] = $arr;
        }

        // render the template, clear message and return
        $output = $app['twig']->render($template, $args);
        SessionManager::clearMessage($app['session']);
        return $output;
    }

    /**
     * Processes picture upload and if successful, updates the user's profile picture.
     * @param Request $request
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processPictureUploadAction(Request $request, Application $app)
    {
        // set up the path where the picture will be saved
        $uploadPath = __DIR__ . '/../../public/pictures/';
        $picture = $request->files->get('picture');
        // if no picture was uploaded, set error message and redirect
        if (!$picture) {
            $message = 'Error! No file uploaded.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/profile');
        }

        // get the type of the file uploaded
        $type = $picture->getMimeType();
        // if type of file doesn't start with 'image', set error message and redirect
        if (strpos($type, 'image') !== 0) {
            $message = 'Error! The file is not a valid image file.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/profile');
        }

        // get the logged in user and their id
        $user = SessionManager::getCurrentUser($app['session']);
        $userId = $user->getId();

        $filename = $picture->getClientOriginalName();

        // try to move the file to /public/pictures
        // picture saved as <user id><file name>.<extension>
        // if saving the picture fails, set error message and redirect
        if (!move_uploaded_file($picture->getPathName(), $uploadPath . $userId . $filename)) {
            $message = 'Error! Image upload failed.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/profile');
        }

        // if the user has a profile picture already, delete it
        if ($curr = $user->getPicture()) {
            $currentPicture = $uploadPath . $curr;
            unlink($currentPicture);
        }

        // set the user's profile picture to the newly uploaded picture
        $user->setPicture($userId . $filename);

        // try to update the user
        $updateResult = User::update($user);

        // if update fails, set error message and redirect
        if (!$updateResult) {
            $message = 'Error! Failed to update the database.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/profile');
        }

        // set success message and redirect
        $message = 'Image upload successful.';
        SessionManager::setMessage($message, Message::SUCCESS, $app['session']);
        return $app->redirect('/profile');
    }

    /**
     * Processes a change of password for a user.
     * @param Request $request
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processChangePasswordAction(Request $request, Application $app)
    {
        $referer = $request->headers->get('referer');
        // get the form data submitted
        $currentPassword = addslashes($request->get('currentPassword'));
        $newPassword1 = addslashes($request->get('newPassword1'));
        $newPassword2 = addslashes($request->get('newPassword2'));

        // save submitted data in form data
        SessionManager::addFormData('changePasswordCurrent', $currentPassword, $app['session']);
        SessionManager::addFormData('changePasswordNew1', $newPassword1, $app['session']);
        SessionManager::addFormData('changePasswordNew2', $newPassword2, $app['session']);

        // try to validate all submitted data
        try {
            User::validatePassword($currentPassword);
            User::validatePassword($newPassword1);
            User::validatePassword($newPassword2);
        } catch (\Exception $e) {
            // if validation fails, set error message and redirect back to user's profile
            $message = 'Error! ' . $e->getMessage();
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/profile');
        }

        // get the logged in user's password
        $user = SessionManager::getCurrentUser($app['session']);
        $userPassword = $user->getPassword();

        // verify user password
        if (!password_verify($currentPassword, $userPassword)) {
            $message = 'Incorrect password.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // if the new password and confirmed new password don't match, set error message and redirect
        if (strcmp($newPassword1, $newPassword2) !== 0) {
            $message = 'The new password values do not match.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        $hashedPassword = password_hash($newPassword1, PASSWORD_BCRYPT);

        // set the new user password
        $user->setPassword($hashedPassword);

        // try to update the user profile
        $updateResult = User::update($user);

        // if update failed, set error message and redirect
        if (!$updateResult) {
            $message = 'Password update failed.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // clear form data
        SessionManager::addFormData('changePasswordCurrent', null, $app['session']);
        SessionManager::addFormData('changePasswordNew1', null, $app['session']);
        SessionManager::addFormData('changePasswordNew2', null, $app['session']);

        // set success message and redirect
        $message = 'Password updated successfully.';
        SessionManager::setMessage($message, Message::SUCCESS, $app['session']);
        return $app->redirect($referer);
    }

    /**
     * Processes a vote on a post.
     * @param Request $request
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processPostVoteAction(Request $request, Application $app)
    {
        $referer = $request->headers->get('referer');
        // get the submitted data
        $upvote = addslashes($request->get('upvote'));
        $downvote = addslashes($request->get('downvote'));
        $postId = addslashes($request->get('postId'));

        // get the logged in user
        $user = SessionManager::getCurrentUser($app['session']);

        // if no user logged in, set error message and redirect
        if (!$user) {
            $message = 'You must log in before you can vote.';
            SessionManager::setMessage($message, Message::WARNING, $app['session']);
            return $app->redirect('/login');
        }

        // if neither upvote nor downvote submitted, or no post is submitted, set error message and redirect
        if (!$upvote && !$downvote) {
            $message = 'No vote submitted';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        } elseif (!$postId) {
            $message = 'Data submitted is incomplete.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }
        // if upvote is set, vote is one; otherwise vote is 0
        $vote = ($upvote) ? 1 : 0;

        // validate the post vote
        try {
            PostVote::validateVote($vote);
        } catch (\Exception $e) {
            // if validation fails, set error message and redirect
            $message = 'Error! ' . $e->getMessage();
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // get id of logged in user
        $userId = $user->getId();

        // get info on post being voted on
        $post = Post::getOneById($postId);
        $postAuthorId = $post->getPostBy();
        $postAuthor = User::getOneById($postAuthorId);

        // if post not found, set error message and redirect
        if (!$post) {
            $message = 'Incorrect form data submitted.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // if the user voting on the post is the user who posted it, set error message and redirect
        if ($postAuthorId == $userId) {
            $message = 'You can\'t vote on your own post.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // find out if the user has already voted on the post
        $postVote = PostVote::searchMultipleColumns(array('user', 'post'), array($userId, $postId));

        // if user hasn't voted on the post, create a new post vote and try to insert it into database
        if (!$postVote) {
            $postVote = new PostVote();
            $postVote->setUser($userId);
            $postVote->setPost($postId);
            $postVote->setVote($vote);
            $dbResult = PostVote::insert($postVote);
        } else {
            // if user has voted on post, check if it was the same as the vote being process now
            $currentVote = $postVote[0]->getVote();
            // if previous vote is same as current vote, remove the previous vote
            // so, if user upvotes post and the clicks upvote again, the upvote is removed
            if ($vote == $currentVote) {
                $dbResult = PostVote::delete($postVote[0]->getId());
            } else {
                // if previous vote was different to current vote, change the vote to current vote
                $postVote[0]->setVote($vote);
                $dbResult = PostVote::update($postVote[0]);
            }
        }

        // if the insertion or update failed, set error message and redirect
        if (!$dbResult) {
            $message = 'A database error occurred.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // if vote was a downvote, the post author loses reputation
        if ($vote === 0) {
            $postAuthor->loseReputation();
        } elseif ($vote === 1) {
            // otherwise, the post author gains reputation
            $postAuthor->addReputation();
        }
        // update the post author with new reputation
        User::update($postAuthor);

        // redirect
        return $app->redirect($referer);
    }

    /**
     * Processes a vote on a thread.
     * @param Request $request
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processThreadVoteAction(Request $request, Application $app)
    {
        $referer = $request->headers->get('referer');
        // get the submitted data
        $upvote = addslashes($request->get('upvote'));
        $downvote = addslashes($request->get('downvote'));
        $threadId = addslashes($request->get('threadId'));

        // get the logged in user
        $user = SessionManager::getCurrentUser($app['session']);

        // if no user logged in, set error message and redirect to login page
        if (!$user) {
            $message = 'You must log in before you can vote.';
            SessionManager::setMessage($message, Message::WARNING, $app['session']);
            return $app->redirect('/login');
        }

        // if neither upvote nor downvote submitted, or no thread id submitted, set error message and redirect
        if (!$upvote && !$downvote) {
            $message = 'No vote submitted';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        } elseif (!$threadId) {
            $message = 'Data submitted is incomplete.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }
        // set vote value
        $vote = ($upvote) ? 1 : 0;

        // validate vote value
        try {
            ThreadVote::validateVote($vote);
        } catch (\Exception $e) {
            // if validation fails, set error message and redirect
            $message = 'Error! ' . $e->getMessage();
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // get logged in user's id
        $userId = $user->getId();

        // get info on thread being voted on
        $thread = Thread::getOneById($threadId);
        $threadAuthorId = $thread->getThreadBy();
        $threadAuthor = User::getOneById($threadAuthorId);

        // if thread not found, set error message and redirect
        if (!$thread) {
            $message = 'Incorrect form data submitted.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // if thread author is the one voting on the thread, set error message and redirect
        if ($thread->getThreadBy() == $userId) {
            $message = 'You can\'t vote on your own thread.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // get thread vote object
        $threadVote = ThreadVote::searchMultipleColumns(array('thread', 'user'), array($threadId, $userId));

        // if the user hasn't voted on the thread, create a new thread vote object
        if (!$threadVote) {
            $threadVote = new ThreadVote();
            $threadVote->setUser($userId);
            $threadVote->setThread($threadId);
            $threadVote->setVote($vote);
            // try to insert the new thread vote into database
            $dbResult = ThreadVote::insert($threadVote);
        } else {
            // if user has voted on the thread, check what their vote was
            $currentVote = $threadVote[0]->getVote();
            // if vote was same as current vote, remove it
            if ($vote == $currentVote) {
                $dbResult = ThreadVote::delete($threadVote[0]->getId());
            } else {
                // otherwise, change the old vote to the new
                $threadVote[0]->setVote($vote);
                $dbResult = ThreadVote::update($threadVote[0]);
            }
        }

        // if database query failed, set error message and redirect
        if (!$dbResult) {
            $message = 'A database error occurred.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // if vote is a downvote, thread author loses reputation
        if ($vote === 0) {
            $threadAuthor->loseReputation();
        } elseif ($vote === 1) {
            // otherwise, thread author gains reputation
            $threadAuthor->addReputation();
        }
        // update the thread author with new reputation value
        User::update($threadAuthor);

        // redirect
        return $app->redirect($referer);
    }

    /**
     * Displays the page with form for deleting an account.
     * @param Request $request
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAccountAction(Request $request, Application $app)
    {
        // get session args
        $args = SessionManager::addSessionArguments($app['session']);
        $template = 'deleteAccount.html.twig';
        // get the logged in user
        $user = SessionManager::getCurrentUser($app['session']);

        // if no user logged in, set error message and redirect to login page
        if (!$user) {
            $message = 'You must be logged in to delete your account.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/login');
        }

        // render template, clear message and return
        $output = $app['twig']->render($template, $args);
        SessionManager::clearMessage($app['session']);
        return $output;
    }

    /**
     * Processes account deletion.
     * @param Request $request
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processDeleteAccountAction(Request $request, Application $app)
    {
        // get path where user profile pictures are stored
        $uploadPath = __DIR__ . '/../../public/pictures/';
        $referer = $request->headers->get('referer');
        // get the password submitted
        $password = addslashes($request->get('password'));

        // store the password in session
        SessionManager::addFormData('deleteAccountPassword', $password, $app['session']);

        // try to validate the password
        try {
            User::validatePassword($password);
        } catch (\Exception $e) {
            // if validation fails, set error message and redirect
            $message = 'Error! ' . $e->getMessage();
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // get the logged in user
        $user = SessionManager::getCurrentUser($app['session']);
        $userId = $user->getId();
        $currentUserPassword = $user->getPassword();

        // get logged in user's picture
        $picture = $user->getPicture();

        // verify password
        if (!password_verify($password, $currentUserPassword)) {
            $message = 'Incorrect password.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // try to delete user
        $deleteResult = User::delete($userId);

        // if deletion failed, set error message and redirect
        if (!$deleteResult) {
            $message = 'A database error occurred.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            $app->redirect($referer);
        }

        // get the full path of user's profile picture
        $profilePicture = $uploadPath . $picture;
        // delete the picture
        unlink($profilePicture);

        // clear form data
        SessionManager::addFormData('deleteAccountPassword', null, $app['session']);

        // set success message and redirect
        $message = 'Your account has been deleted.';
        SessionManager::setMessage($message, Message::INFO, $app['session']);
        return $app->redirect('/logout');
    }
}
