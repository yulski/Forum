<?php

/**
 * namespace of controller classes
 */
namespace Forum\Controller;

use Forum\Table\ForumDBTable;
use Forum\Table\ThreadVote;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Forum\Table\Categorie as Category;
use Forum\Table\User;
use Forum\Table\Thread;
use Forum\Table\Post;
use Forum\Table\PostVote;
use Forum\Util\SessionManager;
use Forum\Util\Message;

/**
 * Class ForumController - controls actions related to creating, editing and deleting parts of the forum.
 * @package Forum\Controller
 */
class ForumController
{

    /**
     * Displays a category and all threads in that category.
     * @param Request $request
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function categoryAction(Request $request, Application $app)
    {
        // get the category template
        $template = 'category.html.twig';

        // get referer and if it doesn't exist, set it to categories page
        $referer = $request->headers->get('referer');
        if (!$referer) {
            $referer = '/categories';
        }

        //  get the category id from the url and save it in session
        $categoryId = addslashes($request->attributes->get('catId'));
        SessionManager::setCategoryId($categoryId, $app['session']);

        // get the category
        $category = Category::getOneById($categoryId);
        // if the category doesn't exist, set message and redirect
        if (!$category) {
            $message = 'Error! This category doesn\'t exist.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // get session arguments
        $args = SessionManager::addSessionArguments($app['session']);
        $args['description'] = $category->getDescription();

        // search for threads in the current category
        $threadsResults = Thread::searchByColumn('category', $categoryId);

        // if there are threads in the category
        if ($threadsResults) {
            // save an array as threads in $args
            $args['threads'] = array();
            //loop through all threads in the category
            foreach ($threadsResults as $thread) {
                // create an array
                $arr = array();
                // get the id of thread and save it in array
                $id = $thread->getId();
                $arr['id'] = $id;
                // get the votes on the thread
                $votes = ThreadVote::searchByColumn('thread', $id);
                // variables to hold upvote and downvote totals
                $upVotes = 0;
                $downVotes = 0;
                // loop through votes
                foreach ($votes as $vote) {
                    // add up votes to get total upvotes and total downvotes
                    if ($vote->getVote() == 0) {
                        $downVotes++;
                    } else {
                        $upVotes++;
                    }
                }
                // if there is a user logged in
                if ($currentUser = SessionManager::getCurrentUser($app['session'])) {
                    // get the user's id
                    $currentUserId = $currentUser->getId();
                    // if the logged in user has voted on the thread
                    if ($currentUserVote = ThreadVote::searchMultipleColumns(array('user', 'thread'), array($currentUserId, $id))) {
                        // if users vote on the thread to array
                        if ($currentUserVote[0]->getVote() == 0) {
                            $arr['currentUserDownvoted'] = true;
                        } elseif ($currentUserVote[0]->getVote() == 1) {
                            $arr['currentUserUpvoted'] = true;
                        }
                    }
                }
                // add total upvotes and downvotes to array
                $arr['upVotes'] = $upVotes;
                $arr['downVotes'] = $downVotes;
                // save thread subject, date created and username of user who created it to array
                $arr['subject'] = $thread->getSubject();
                $arr['dateCreated'] = $thread->getDateCreated();
                $user = User::getOneById($thread->getThreadBy());
                $arr['threadBy'] = $user->getUsername();
                // save profile picture of user who created thread to array
                $picture = $user->getPicture();
                // use default picture if user doesn't have a profile picture
                if (!$picture) {
                    $picture = DEFAULT_PICTURE;
                }
                $arr['profilePicture'] = PICTURE_DIRECTORY_PATH . $picture;
                // add the array to 'threads'. the array represents a single thread
                $args['threads'][] = $arr;
            }
        }

        // render the template and return it
        $output = $app['twig']->render($template, $args);
        SessionManager::clearMessage($app['session']);
        return $output;
    }

    /**
     * Displays a thread and all the posts in it.
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function threadAction(Request $request, Application $app)
    {
        // get template and $args from session
        $template = 'thread.html.twig';
        $args = SessionManager::addSessionArguments($app['session']);

        // get referer and if it doesn't exist, set it to categories page
        $referer = $request->headers->get('referer');
        if (!$referer) {
            $referer = '/categories';
        }

        // get thread id and category name from url
        $threadId = addslashes($request->attributes->get('threadId'));
        SessionManager::setThreadId($threadId, $app['session']);
        $categoryId = addslashes($request->attributes->get('catId'));
        // get the category and if it doesn't exist, set message and redirect
        $category = Category::getOneById($categoryId);
        if (!$category) {
            $message = 'Error! That category doesn\'t exist.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // get the current thread
        $currentThread = Thread::getOneById($threadId);
        // if it doesn't exist, set message and redirect
        if (!$currentThread) {
            $message = 'Error! This thread doesn\'t exist.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // get the id of the thread author
        $userId = $currentThread->getThreadBy();
        // set thread by to username of the thread author rather than id
        $currentThread->setThreadBy(User::getOneById($userId)->getUsername());

        // put current thread in $args
        $args['currentThread'] = array();
        $args['currentThread']['threadBy'] = $currentThread->getThreadBy();
        $args['currentThread']['dateCreated'] = $currentThread->getDateCreated();
        $args['currentThread']['subject'] = $currentThread->getSubject();

        // get the posts on the current thread
        $posts = Post::searchByColumn('postThread', $threadId);

        if ($posts) {
            // loop through the posts
            foreach ($posts as $post) {
                // $arr is an array representing a single post
                $arr = array();
                $id = $post->getId();
                // get the votes for the post
                $votes = PostVote::searchByColumn('post', $id);
                // variables to store upvotes and downvotes on the post
                $upVotes = 0;
                $downVotes = 0;
                // loop through the votes
                foreach ($votes as $vote) {
                    // check if the vote is up or down and add to vote totals accordingly
                    if ($vote->getVote() == 0) {
                        $downVotes++;
                    } else {
                        $upVotes++;
                    }
                }
                // check if a user is logged in
                if ($currentUser = SessionManager::getCurrentUser($app['session'])) {
                    $currentUserId = $currentUser->getId();
                    // check if the logged in user has voted on the post
                    if ($currentUserVote = PostVote::searchMultipleColumns(array('user', 'post'), array($currentUserId, $id))) {
                        // check if the user upvoted or downvoted the post and store it in $arr
                        if ($currentUserVote[0]->getVote() == 0) {
                            $arr['currentUserDownvoted'] = true;
                        } elseif ($currentUserVote[0]->getVote() == 1) {
                            $arr['currentUserUpvoted'] = true;
                        }
                    }
                }
                // store upvote and downvote totals in $arr
                $arr['upVotes'] = $upVotes;
                $arr['downVotes'] = $downVotes;
                $arr['id'] = $id;
                // store post content, date and author's username in $arr
                $arr['content'] = $post->getPostContent();
                $arr['date'] = $post->getPostDate();
                $userId = $post->getPostBy();
                $user = User::getOneById($userId);
                $arr['user'] = $user->getUsername();
                // store post author's profile picture in $arr
                $picture = $user->getPicture();
                // use default picture if post author has no profile picture
                if (!$picture) {
                    $picture = DEFAULT_PICTURE;
                }
                $arr['profilePicture'] = PICTURE_DIRECTORY_PATH . $picture;
                // add $arr as another post in $args['posts']
                $args['posts'][] = $arr;
            }
        }

        // render the template and return
        $output = $app['twig']->render($template, $args);
        SessionManager::clearMessage($app['session']);
        return $output;
    }

    /**
     * Processes thread creation and if all data is valid, creates a new thread.
     * @param Request $request
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processCreateThreadAction(Request $request, Application $app)
    {
        // get the referer
        $referer = $request->headers->get('referer');

        // get the submitted thread subject and save it in form data
        $threadSubject = addslashes($request->get('threadSubject'));
        SessionManager::addFormData('createThreadSubject', $threadSubject, $app['session']);

        // try to validate the thread subject
        try {
            Thread::validateThreadSubject($threadSubject);
        } catch (\Exception $e) {
            // if thread subject not valid, set message and redirect
            $message = 'Error! ' . $e->getMessage();
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // if no user is logged in, set message and redirect to login page
        if (!SessionManager::getCurrentUser($app['session'])) {
            $message = 'You must log in before you can create a new thread.';
            SessionManager::setMessage($message, Message::WARNING, $app['session']);
            return $app->redirect('/login');
        }

        $date = date('Y-m-d');
        // get the category the thread will be posted in
        $categoryId = SessionManager::getCategoryId($app['session']);
        // get the logged in user
        $user = SessionManager::getCurrentUser($app['session']);
        $username = $user->getUsername();
        $userId = User::searchByColumn('username', $username)[0]->getId();

        // search for a thread in the same category with the same subject
        $subjectResults = Thread::searchMultipleColumns(array('subject', 'category'), array($threadSubject, $categoryId), ForumDBTable::SEARCH_LIKE);

        // if the thread exists, set message and redirect
        if (count($subjectResults) !== 0) {
            $message = 'Error! That thread already exists.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // create a new thread to insert into database
        $thread = new Thread();
        $thread->setSubject($threadSubject);
        $thread->setDateCreated($date);
        $thread->setCategory($categoryId);
        $thread->setThreadBy($userId);
        // try to insert the thread
        $insertResult = Thread::insert($thread);


        // if insertion failed, set message and redirect
        if ($insertResult < 0) {
            $message = 'A database error occurred while creating the thread.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // clear thread subject from form data
        SessionManager::addFormData('createThreadSubject', null, $app['session']);

        // set success message and redirect
        $message = 'Thread created successfully';
        SessionManager::setMessage($message, Message::SUCCESS, $app['session']);
        return $app->redirect($referer);
    }

    /**
     * Processes post creation and if all data is valid, creates a new post.
     * @param Request $request
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processCreatePostAction(Request $request, Application $app)
    {

        // get the submitted post content and add it to form data
        $postContent = addslashes($request->get('post'));
        SessionManager::addFormData('createPostContent', $postContent, $app['session']);

        // if no user logged in, set error message and redirect
        if (!SessionManager::getCurrentUser($app['session'])) {
            $message = 'You must log in before you can post.';
            SessionManager::setMessage($message, Message::WARNING, $app['session']);
            return $app->redirect('/login');
        }

        // if post content is empty, set error message and redirect
        if (empty($postContent)) {
            $message = 'Error! Post is empty.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/login');
        }

        $datetime = date('Y-m-d H:i:s');

        // get the logged in user
        $user = SessionManager::getCurrentUser($app['session']);
        $userId = $user->getId();

        // get the category id from session
        $categoryId = SessionManager::getCategoryId($app['session']);

        // get thread subject from session
        $threadId = SessionManager::getThreadId($app['session']);

        // create new post object
        $post = new Post();
        $post->setPostContent($postContent);
        $post->setPostDate($datetime);
        $post->setPostThread($threadId);
        $post->setPostBy($userId);

        // try to insert the post into the database
        $insertResult = Post::insert($post);

        // if insertion failed, set error message and redirect
        if (!$insertResult) {
            $message = 'Error! An error occurred when submitting your post.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/categories/ ' . $categoryId . '/' . $threadId);
        }

        // clear post content from form data
        SessionManager::addFormData('createPostContent', null, $app['session']);

        $referer = '/categories/' . $categoryId . '/' . $threadId;

        // set success message and redirect
        $message = 'Success! Your post was successfully posted.';
        SessionManager::setMessage($message, Message::SUCCESS, $app['session']);
        return $app->redirect($referer);
    }

    /**
     * Displays the form to delete a thread.
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function deleteThreadAction(Request $request, Application $app)
    {
        // get $args from session and template
        $args = SessionManager::addSessionArguments($app['session']);
        $template = 'deleteThread.html.twig';

        if (!$threadId = addslashes($request->get('threadId'))) {
            $threadId = SessionManager::getThreadId($app['session']);
        }

        SessionManager::setThreadId($threadId, $app['session']);

        $thread = Thread::getOneById($threadId);
        $threadSubject = $thread->getSubject();

        $categoryId = SessionManager::getCategoryId($app['session']);

        // store thread subject in $args
        $args['threadSubject'] = $threadSubject;

        // get the referer and store in $args
        $referer = '/categories/' . $categoryId;
        $args['referer'] = $referer;

        // render the template, clear message and return
        $output = $app['twig']->render($template, $args);
        SessionManager::clearMessage($app['session']);
        return $output;
    }

    /**
     * Processes thread deletion and if data valid, deletes a thread.
     * @param Request $request
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processDeleteThreadAction(Request $request, Application $app)
    {
        // get referer
        $referer = $request->headers->get('referer');

        // get the submitted password and store it in form data
        $password = addslashes($request->get('password'));
        SessionManager::addFormData('deleteThreadPassword', $password, $app['session']);
        // try to validate the password
        try {
            User::validatePassword($password);
        } catch (\Exception $e) {
            // if validation fails, set error message and redirect
            $message = 'Error! ' . $e->getMessage();
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/deleteThread');
        }

        // get the logged in user's password
        $user = SessionManager::getCurrentUser($app['session']);
        $userPassword = $user->getPassword();
        // verify submitted password
        if (!password_verify($password, $userPassword)) {
            $message = 'Incorrect password.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/deleteThread');
        }

        $threadId = SessionManager::getThreadId($app['session']);

        $thread = Thread::getOneById($threadId);

        // if the thread doesn't exist, set error message and redirect
        if (!$thread) {
            $message = 'That thread does not exist.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // if the user is not admin and not the author of the thread, set error message and redirect
        if (!$user->getAdministrator() && $thread->getThreadBy() !== $user->getId()) {
            $message = 'You are not the author of this thread and cannot edit it.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // try to delete the thread
        $deleteResult = Thread::delete($threadId);

        // if deletion fails, set error message and redirect
        if (!$deleteResult) {
            $message = 'Error occurred when trying to delete the thread.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        $categoryId = SessionManager::getCategoryId($app['session']);

        // clear password from form data
        SessionManager::addFormData('deleteThreadPassword', null, $app['session']);

        // set success message and redirect
        $message = 'Thread successfully deleted.';
        SessionManager::setMessage($message, Message::SUCCESS, $app['session']);
        return $app->redirect('/categories/' . $categoryId);
    }

    /**
     * Displays the page with form for deleting a post.
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function deletePostAction(Request $request, Application $app)
    {
        // get session args and template
        $args = SessionManager::addSessionArguments($app['session']);
        $template = 'deletePost.html.twig';

        $categoryId = SessionManager::getCategoryId($app['session']);
        $threadId = SessionManager::getThreadId($app['session']);

        // get referer and store it in $args
        $referer = '/categories/' . $categoryId . '/' . $threadId;

        $args['referer'] = $referer;

        // get submitted post content, post date and post author
        $postContent = addslashes($request->get('postContent'));
        $postDate = addslashes($request->get('postDate'));
        $postUser = addslashes($request->get('postUser'));

        // if any of the submitted data is empty, set error message, render template and return
        if (empty($postContent) || empty($postDate) || empty($postUser)) {
            $message = 'Error! Submitted form data was incomplete.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            $output = $app['twig']->render($template, $args);
            SessionManager::clearMessage($app['session']);
            return $output;
        }

        // store post in session
        SessionManager::setPost($postContent, $postDate, $postUser, $app['session']);

        // render template, clear message and return
        $output = $app['twig']->render($template, $args);
        SessionManager::clearMessage($app['session']);
        return $output;
    }

    /**
     * Processes post deletion and if data is valid, deletes a post.
     * @param Request $request
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processDeletePostAction(Request $request, Application $app)
    {
        // get the submitted password and store it in form data
        $password = addslashes($request->get('password'));
        SessionManager::addFormData('deletePostPassword', $password, $app['session']);

        // try to validate the password
        try {
            User::validatePassword($password);
        } catch (\Exception $e) {
            // if validation fails, set error message and redirect
            $message = 'Error! ' . $e->getMessage();
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/deletePost');
        }

        // get the logged in user's password
        $user = SessionManager::getCurrentUser($app['session']);
        $userPassword = $user->getPassword();
        // verify submitted password
        if (!password_verify($password, $userPassword)) {
            $message = 'Incorrect password.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/deletePost');
        }

        // get category name, thread subject and post from session
        $post = SessionManager::getPost($app['session']);
        $postContent = $post['content'];
        $postDate = $post['date'];
        $postUser = $post['user'];

        // get id of post author
        $userId = User::searchByColumn('username', $postUser)[0]->getId();

        // find post id
        $categoryId = SessionManager::getCategoryId($app['session']);
        $threadId = SessionManager::getThreadId($app['session']);
        $postId = Post::searchMultipleColumns(array('postContent', 'postDate', 'postThread', 'postBy'), array($postContent, $postDate, $threadId, $userId))[0]->getId();

        $referer = '/categories/' . $categoryId . '/' . $threadId;

        // if logged in user isn't admin or post author, set error message and redirect
        if (!$user->getAdministrator() && $userId !== $user->getId()) {
            $message = 'You are not the author of that post and cannot delete it.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }



        // try to delete the post
        $deleteResult = Post::delete($postId);

        // if deletion failed, set error message and redirect
        if (!$deleteResult) {
            $message = 'Error occurred when trying to delete the post.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // clear password from form data
        SessionManager::addFormData('deletePostPassword', null, $app['session']);

        // set success message and redirect
        $message = 'Post successfully deleted.';
        SessionManager::setMessage($message, Message::SUCCESS, $app['session']);
        return $app->redirect($referer);
    }

    /**
     * Displays page with form for editing a thread
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function editThreadAction(Request $request, Application $app)
    {
        // get session args and template
        $args = SessionManager::addSessionArguments($app['session']);
        $template = 'editThread.html.twig';

        if (!$threadId = addslashes($request->get('threadId'))) {
            $threadId = SessionManager::getThreadId($app['session']);
        }
        $thread = Thread::getOneById($threadId);
        $threadSubject = $thread->getSubject();

        // store thread subject in $args and in session
        $args['threadSubject'] = $threadSubject;
        SessionManager::setThreadId($threadId, $app['session']);

        $categoryId = SessionManager::getCategoryId($app['session']);

        // get referer and store it in $args
        $referer = '/categories/' . $categoryId;
        $args['referer'] = $referer;

        // render template, clear message and return
        $output = $app['twig']->render($template, $args);
        SessionManager::clearMessage($app['session']);
        return $output;
    }

    /**
     * Processes thread edition and if data is valid, edits the thread.
     * @param Request $request
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processEditThreadAction(Request $request, Application $app)
    {
        // get the submitted data and store in session
        $password = addslashes($request->get('password'));
        $newName = addslashes($request->get('newName'));
        SessionManager::addFormData('editThreadPassword', $password, $app['session']);
        SessionManager::addFormData('editThreadSubject', $newName, $app['session']);
        // try to validate password
        try {
            User::validatePassword($password);
        } catch (\Exception $e) {
            // if validation fails, set error message and redirect
            $message = 'Error! ' . $e->getMessage();
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/editThread');
        }

        // get logged in user's password
        $user = SessionManager::getCurrentUser($app['session']);
        $userPassword = $user->getPassword();
        // verify submitted password
        if (!password_verify($password, $userPassword)) {
            $message = 'Incorrect password.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/editThread');
        }

        // try to validate thread subject
        try {
            Thread::validateThreadSubject($newName);
        } catch (\Exception $e) {
            // if validation fails, set error message and redirect
            $message = 'Error! ' . $e->getMessage();
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/editThread');
        }

        // get referer
        $referer = $request->headers->get('referer');

        // get category id
        $categoryId = SessionManager::getCategoryId($app['session']);

        // get the thread and set it's name to the new value
        $threadId = SessionManager::getThreadId($app['session']);
        $thread = Thread::getOneById($threadId);
        $thread->setSubject($newName);

        // if the logged in user isn't admin or thread author, set error message and redirect
        if (!$user->getAdministrator() && $thread->getThreadBy() !== $user->getId()) {
            $message = 'You are not the author of this thread and cannot edit it.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // try to update thread
        $updateResult = Thread::update($thread);

        // if update fails, set error message and redirect
        if (!$updateResult) {
            $message = 'Error occurred when trying to update the thread name.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // clear form data
        SessionManager::addFormData('editThreadPassword', null, $app['session']);
        SessionManager::addFormData('editThreadSubject', null, $app['session']);

        // set success message and redirect back to category
        $message = 'Thread successfully updated.';
        SessionManager::setMessage($message, Message::SUCCESS, $app['session']);
        return $app->redirect('/categories/' . $categoryId);
    }

    /**
     * Displays the form for post edition.
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function editPostAction(Request $request, Application $app)
    {
        // get session args and template
        $args = SessionManager::addSessionArguments($app['session']);
        $template = 'editPost.html.twig';

        // get submitted post data
        $postContent = addslashes($request->get('postContent'));
        $postDate = addslashes($request->get('postDate'));
        $postUser = addslashes($request->get('postUser'));

        $categoryId = SessionManager::getCategoryId($app['session']);
        $threadId = SessionManager::getThreadId($app['session']);

        // store referer in $args
        $referer = '/categories/' . $categoryId . '/' . $threadId;
        $args['referer'] = $referer;

        // try to validate the username
        try {
            User::validateUsername($postUser);
            // make sure post content is non-empty
            if (empty($postContent) || empty($postDate)) {
                throw new \Exception('Empty data submitted.');
            }
        } catch (\Exception $e) {
            // if validation fails, set error message, render template and return
            $message = 'Error! ' . $e->getMessage();
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            $output = $app['twig']->render($template, $args);
            SessionManager::clearMessage($app['session']);
            return $output;
        }

        $args['postContent'] = $postContent;
        // store post in session
        SessionManager::setPost($postContent, $postDate, $postUser, $app['session']);

        // render template, clear message and return
        $output = $app['twig']->render($template, $args);
        SessionManager::clearMessage($app['session']);
        return $output;
    }

    /**
     * Processes post edition and if data is valid, edits the post.
     * @param Request $request
     * @param Application $app
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function processEditPostAction(Request $request, Application $app)
    {
        // store the submitted password and content in form data
        $password = addslashes($request->get('password'));
        $newContent = addslashes($request->get('newContent'));
        SessionManager::addFormData('editPostPassword', $password, $app['session']);
        SessionManager::addFormData('editPostContent', $newContent, $app['session']);
        // try to validate the password
        try {
            User::validatePassword($password);
        } catch (\Exception $e) {
            // if validation fails, set error message and redirect
            $message = 'Error! ' . $e->getMessage();
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/editPost');
        }
        // get the logged in user's password
        $user = SessionManager::getCurrentUser($app['session']);
        $userPassword = $user->getPassword();
        // verify password
        if (!password_verify($password, $userPassword)) {
            $message = 'Incorrect password.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/editPost');
        }

        // if new content is empty, set error message and redirect
        if (empty($newContent)) {
            $message = 'Error! You cannot make the post empty.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect('/editPost');
        }

        $referer = $request->headers->get('referer');

        // get category, thread and post from session

        $post = SessionManager::getPost($app['session']);
        $postContent = $post['content'];
        $postDate = $post['date'];
        $postUser = $post['user'];

        $userId = User::searchByColumn('username', $postUser)[0]->getId();

        $categoryId = SessionManager::getCategoryId($app['session']);

        // get the id of the post
        $threadId = SessionManager::getThreadId($app['session']);
        $postId = Post::searchMultipleColumns(array('postContent', 'postDate', 'postThread', 'postBy'), array($postContent, $postDate, $threadId, $userId))[0]->getId();

        // get the post that is to be edited
        $post = Post::getOneById($postId);

        // if user isn't admin or post author, set error message and redirect
        if (!$user->getAdministrator() && $post->getPostBy() !== $user->getId()) {
            $message = 'You are not the author of this post and cannot edit it.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // set new post content
        $post->setPostContent($newContent);

        // try to update the post
        $updateResult = Post::update($post);

        // if update fails, set error message and redirect
        if (!$updateResult) {
            $message = 'Error occurred when trying to update the post.';
            SessionManager::setMessage($message, Message::ERROR, $app['session']);
            return $app->redirect($referer);
        }

        // clear session data
        SessionManager::addFormData('editPostPassword', null, $app['session']);
        SessionManager::addFormData('editPostContent', null, $app['session']);

        // set success message and redirect back to thread
        $message = 'Post successfully updated.';
        SessionManager::setMessage($message, Message::SUCCESS, $app['session']);
        return $app->redirect('/categories/' . $categoryId . '/' . $threadId);
    }
}
