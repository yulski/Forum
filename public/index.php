<?php

require_once __DIR__ . '/../app/setup.php';

// main controller actions
$app->get('/', 'Forum\Controller\MainController::indexAction');
$app->get('/login', 'Forum\Controller\MainController::loginAction');
$app->get('/logout', 'Forum\Controller\UserController::logoutAction');
$app->get('/register', 'Forum\Controller\MainController::registerAction');
$app->get('/categories', 'Forum\Controller\MainController::categoriesAction');
$app->post('/processSearch', 'Forum\Controller\MainController::processSearchAction');

// forum controller actions
$app->get('/categories/{catId}', 'Forum\Controller\ForumController::categoryAction');
$app->post('/processCreateThread', 'Forum\Controller\ForumController::processCreateThreadAction');
$app->get('/categories/{catId}/{threadId}', 'Forum\Controller\ForumController::threadAction');
$app->post('/processCreatePost', 'Forum\Controller\ForumController::processCreatePostAction');
$app->get('/editThread', 'Forum\Controller\ForumController::editThreadAction');
$app->post('/processEditThread', 'Forum\Controller\ForumController::processEditThreadAction');
$app->get('/editPost', 'Forum\Controller\ForumController::editPostAction');
$app->post('/processEditPost', 'Forum\Controller\ForumController::processEditPostAction');
$app->get('/deleteThread', 'Forum\Controller\ForumController::deleteThreadAction');
$app->post('/processDeleteThread', 'Forum\Controller\ForumController::processDeleteThreadAction');
$app->get('/deletePost', 'Forum\Controller\ForumController::deletePostAction');
$app->post('/processDeletePost', 'Forum\Controller\ForumController::processDeletePostAction');

// user controller actions
$app->post('/processLogin', 'Forum\Controller\UserController::processLoginAction');
$app->post('/processRegistration', 'Forum\Controller\UserController::processRegistrationAction');
$app->get('/profile', 'Forum\Controller\UserController::userProfileAction');
$app->post('/processPictureUpload', 'Forum\Controller\UserController::processPictureUploadAction');
$app->post('/processChangePassword', 'Forum\Controller\UserController::processChangePasswordAction');
$app->post('/processPostVote', 'Forum\Controller\UserController::processPostVoteAction');
$app->post('/processThreadVote', 'Forum\Controller\UserController::processThreadVoteAction');
$app->get('/deleteAccount', 'Forum\Controller\UserController::deleteAccountAction');
$app->post('/processDeleteAccount', 'Forum\Controller\UserController::processDeleteAccountAction');

// admin controller actions
$app->get('/createCategory', 'Forum\Controller\AdminController::createCategoryAction');
$app->post('/processCreateCategory', 'Forum\Controller\AdminController::processCreateCategoryAction');
$app->get('/deleteCategory', 'Forum\Controller\AdminController::deleteCategoryAction');
$app->post('/processDeleteCategory', 'Forum\Controller\AdminController::processDeleteCategoryAction');
$app->get('/editCategory', 'Forum\Controller\AdminController::editCategoryAction');
$app->post('/processEditCategory', 'Forum\Controller\AdminController::processEditCategoryAction');

$app->run();
