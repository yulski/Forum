function adjustFooter() {
    var footer = $('footer')[0],
        windowHeight = $(window).height(),
        documentHeight = $(document).height();

    if(documentHeight === windowHeight && !$(footer).hasClass('stick-to-bottom')) {
        $(footer).addClass('stick-to-bottom');
    } else if(documentHeight > windowHeight && $(footer).hasClass('stick-to-bottom')) {
        $(footer).removeClass('stick-to-bottom');
    }
}
