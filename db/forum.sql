-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 21, 2016 at 05:11 PM
-- Server version: 5.6.28-0ubuntu0.15.10.1
-- PHP Version: 5.6.11-1ubuntu3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `forum`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`) VALUES
(1, 'Web Development', 'Discussion about web development. HTML5, CSS, Javascript, PHP - it is all here.'),
(2, 'Martial arts', 'Talk about martial arts, sport and self defense, white and black belt.');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(11) NOT NULL,
  `postContent` text NOT NULL,
  `postDate` datetime NOT NULL,
  `postThread` int(11) NOT NULL,
  `postBy` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `postContent`, `postDate`, `postThread`, `postBy`) VALUES
(1, 'create a getSession function. it should return a new Session(new MockArraySessionStorage()). use that returned session for session storage in your tests :-)', '2016-04-21 17:03:06', 2, 1),
(2, 'great thanks!', '2016-04-21 17:03:21', 2, 2),
(3, 'the average is 10 years', '2016-04-21 17:10:57', 4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `postvotes`
--

CREATE TABLE IF NOT EXISTS `postvotes` (
  `id` int(11) NOT NULL,
  `post` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `vote` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `postvotes`
--

INSERT INTO `postvotes` (`id`, `post`, `user`, `vote`) VALUES
(1, 1, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `threads`
--

CREATE TABLE IF NOT EXISTS `threads` (
  `id` int(11) NOT NULL,
  `subject` varchar(256) NOT NULL,
  `dateCreated` date NOT NULL,
  `category` int(11) NOT NULL,
  `threadBy` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `threads`
--

INSERT INTO `threads` (`id`, `subject`, `dateCreated`, `category`, `threadBy`) VALUES
(1, 'What is Silex and why use it', '2016-04-21', 1, 1),
(2, 'How to unit test Symfony sessions', '2016-04-21', 1, 2),
(3, 'asdsadasdasd..', '2016-04-21', 1, 1),
(4, 'How long to get BJJ black belt', '2016-04-21', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `threadvotes`
--

CREATE TABLE IF NOT EXISTS `threadvotes` (
  `id` int(11) NOT NULL,
  `thread` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `vote` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `threadvotes`
--

INSERT INTO `threadvotes` (`id`, `thread`, `user`, `vote`) VALUES
(1, 1, 2, 1),
(2, 2, 1, 1),
(3, 3, 2, 0),
(4, 2, 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `username` varchar(32) NOT NULL,
  `email` varchar(256) NOT NULL,
  `password` varchar(60) NOT NULL,
  `dateJoined` date NOT NULL,
  `administrator` tinyint(4) NOT NULL,
  `reputation` int(11) NOT NULL DEFAULT '0',
  `picture` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `dateJoined`, `administrator`, `reputation`, `picture`) VALUES
(1, 'julek14', 'karski.julek@yahoo.com', '$2y$10$nJIZ3fdv.5kIz1JwC/X8KO3BKVsVTRBO7nzjbYUin4RsHwsHJTzYy', '2016-04-21', 1, 1, '1admin.png'),
(2, 'marley', 'marley.bob@gmail.com', '$2y$10$1oJmafHGXTlTvN0cpBkoleqtuSCUhVEp.mlpMJVIsEH2e1slA/3K2', '2016-04-21', 0, 2, '2bob.jpg'),
(3, 'anotherUser', 'another@yahoo.com', '$2y$10$1hx0Tl8E0odVF2TDy73jFODuShd3gU.xPps/xjreA1TvpwNP5iBXe', '2016-04-21', 0, 0, '3java_mug.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Post_Thread` (`postThread`),
  ADD KEY `FK_Post_User` (`postBy`);

--
-- Indexes for table `postvotes`
--
ALTER TABLE `postvotes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Constraint_PostVote_Post_FK` (`post`),
  ADD KEY `Constraint_PostVote_User_FK` (`user`);

--
-- Indexes for table `threads`
--
ALTER TABLE `threads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_Thread_Category` (`category`),
  ADD KEY `FK_Thread_By` (`threadBy`);

--
-- Indexes for table `threadvotes`
--
ALTER TABLE `threadvotes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Constraint_ThreadVote_Post_FK` (`thread`),
  ADD KEY `Constraint_ThreadVote_User_FK` (`user`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `postvotes`
--
ALTER TABLE `postvotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `threads`
--
ALTER TABLE `threads`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `threadvotes`
--
ALTER TABLE `threadvotes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `FK_Post_Thread` FOREIGN KEY (`postThread`) REFERENCES `threads` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Post_User` FOREIGN KEY (`postBy`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `postvotes`
--
ALTER TABLE `postvotes`
  ADD CONSTRAINT `Constraint_PostVote_Post_FK` FOREIGN KEY (`post`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Constraint_PostVote_User_FK` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `threads`
--
ALTER TABLE `threads`
  ADD CONSTRAINT `FK_Thread_By` FOREIGN KEY (`threadBy`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_Thread_Category` FOREIGN KEY (`category`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `threadvotes`
--
ALTER TABLE `threadvotes`
  ADD CONSTRAINT `Constraint_ThreadVote_Post_FK` FOREIGN KEY (`thread`) REFERENCES `threads` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Constraint_ThreadVote_User_FK` FOREIGN KEY (`user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
